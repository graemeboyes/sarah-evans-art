<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

class AutonavExcludeSubpagesPackage extends Package {
	
	protected $pkgHandle = 'autonav_exclude_subpages';
	protected $appVersionRequired = '5.4.1';
	protected $pkgVersion = '1.1';
	
	public function getPackageName() {
		return t("Autonav Exclude Subpages"); 
	}	
	
	public function getPackageDescription() {
		return t("Custom template for the Autonav block that improves 'exclude from nav' functionality and also allows you to 'exclude subpages from nav'.");
	}

	public function install() {
		$pkg = parent::install();

		$this->installExcludeSubpageAttribute();
	}
	
	public function upgrade() {
		parent::upgrade();
		
		// 1.1
		$this->installExcludeSubpageAttribute();
	}
	
	private function installExcludeSubpageAttribute() {
		Loader::model('collection_attributes');
		$excludeSubpagesAttrKey = CollectionAttributeKey::getByHandle('exclude_subpages_from_nav');
		if (!$excludeSubpagesAttrKey || !intval($excludeSubpagesAttrKey>getAttributeKeyID())) {
			$boolt = AttributeType::getByHandle('boolean');
			$excludeSubpagesAttrKey = CollectionAttributeKey::add($boolt, array('akHandle' => 'exclude_subpages_from_nav', 'akName' => t('Exclude Subpages from Nav'), 'akIsSearchable' => false));
		}	
	}

}