��    =        S   �      8  3   9     m     |  %   �     �     �  
   �     �     �     �     �            \   "  !     !   �  	   �     �     �     �  
             &     ,     1  "   9     \     n     �     �  	   �     �     �  
   �  ,   �  (        -     E     X  2   k     �     �  	   �     �  	   �     �     �     �     	     	     	  ?   	  ?   ^	     �	     �	     �	  2   �	     �	     
     
  g  
  O        �     �  4   �  *        D     P     ]     q     }     �     �     �  m   �  5      )   V     �     �     �     �     �     �     	            4        T     o  &   �     �  	   �  #   �     �  
   �  2   �  *   0     [     {     �  4   �     �  	   �     �       	   '     1     E  !   K     m     �     �  =   �  =   �          *     0  T   A     �     �     �            *   =       "      7   #         	      9                0                8              ,                  
       '   -   +          6                     (   !          5   ;   %   $          <       2              )                    .   3   :   &               4   /          1           A highly customizable filmstrip style image gallery Access Denied. Auto Custom Button Hover Images (Optional) Custom Button Images (Optional) Dark Embossed Date Added Date Posted Description Design Details &raquo; Ebony Enlarge Mode Enter the file IDs of the images you would like to display first. Seperate them with commas. Enter zero for no page size limit Error: no service mode specified. File Name File Not Found File Sets (optional) Files must be... Film Strip Filter By Tag (optional) Fixed Grid Height: Hide details that haven't been set Image Detail Page Image Details to Display Image Tags Link Page Images Interface Large Image Region Height Left Light Gray Link image tags to another page on your site Link images to another page on your site Max Enlarged Image Size Max Thumbnail Size No file sets found No image interface be displayed with these options None Order By Page Size Preload large images Pro Photo ProPhoto Browser Right Selected Image Color Show paging links Style Tags The page must handle a "fID" parameter in the url's querystring The page must handle a "tag" parameter in the url's querystring Thumbs display Title Transparent Gray Use the %sfile manager%s to place images into sets View Filmstrip Image: Width: px Project-Id-Version: Concrete5 ProPhoto
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-02-06 00:24-0800
PO-Revision-Date: 2011-02-19 09:31+0100
Last-Translator: cali <cali@concrete5.fr>
Language-Team: concrete5 FR <cali@concrete5.fr>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: gettext;gettext_noop;t
X-Poedit-Basepath: /Users/tony/ConcreteCMS/concrete5-addons/trunk/web/packaged_blocks/tony_pro_photo/
X-Poedit-Language: French
Plural-Forms: nplurals=2; plural=n>1;
X-Poedit-Country: FRANCE
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: .
 Une gallerie photo hautement personnalisable avec menu type péllicule de film. Accès refusé. Auto Image personnalisée du bouton au survol (optionnel) Image personnalisée du bouton (optionnel) Noir relief Date d'ajout Date de publication Description Design Détails &raquo; Ebène Mode d'agrandissement Entrer les IDs des fichiers dont vous désirez afficher les images en premier. Séparer les par des virgules. Entrez zéro pour ne pas limiter la taille de la page Erreur: Aucun mode de service spécifié. Nom du fichier Fichier non trouvé Collections (optionnel) Les fichiers doivent êtres ... Pellicule de film Filtrer par tag (optionnel) Fixé Grille Hauteur: Cacher les détails qui n'ont pas été paramétrés Page des détails d'images Détails de l'image à afficher Liens des tags d'images et leurs pages Images Interface Hauteur de la zone d'image agrandie Gauche Gris clair Lier les tags des images à d'autres pages du site Lier les images sur une autre page du site Taille maximum d'image agrandie Taille max des miniatures Aucune collection trouvée Aucune interface image ne s'affiche avec ces options Auncun Trier par Taille de la page Précharger les grandes images Pro Photo Navigateur ProPhoto Droit Couleur de l'image sélectionnée Afficher les liens des pages Style Tags La page doit gérer un paramètre "fID " dans la requête URL La page doit gérer un paramètre "tag " dans la requête URL Affichage des miniatures Titre Gris transparent Utiliser le %sgestionnaire de fichiers%s pour placer vos images dans des collections Voir la bande péllicule: Largeur: px 