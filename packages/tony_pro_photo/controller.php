<?php 

defined('C5_EXECUTE') or die(_("Access Denied."));

/*
Optional Global Variables: 

PRO_PHOTO_FILE_PERMISSIONS_DISABLED: true or false

Note that these global variables will override any block specific settings
You must clear you cache for your changes to take effect
WATERMARK_FONT_ALPHA: 0-127, default=0 
WATERMARK_FONT_SIZE: default=0
WATERMARK_POSITION: tl, tr, bl, or br (default)
WATERMARK_TYPE: text, none, or image
WATERMARK_FID: 
WATERMARK_THUMBS: 1 or 0 
WATERMARK_THUMBS_TEXT: text string 
WATERMARK_TEXT: text string 

*/

class TonyProPhotoPackage extends Package {

	protected $pkgHandle = 'tony_pro_photo';
	protected $appVersionRequired = '5.3.2';
	protected $pkgVersion = '2.02';  
	
	public function getPackageDescription() { 
		return t("A highly customizable filmstrip style image gallery"); 
	}
	
	public function getPackageName() {
		return t("ProPhoto Browser");
	}
	
	public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('tony_pro_photo', $pkg);
		
	}


}