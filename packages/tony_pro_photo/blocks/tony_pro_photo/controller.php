<?php  

defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('file_list');
Loader::model('file_set');
Loader::model('file_attributes'); 
Loader::model('user_attributes'); 
//Loader::model('collection_attributes'); 

class TonyProPhotoBlockController extends BlockController { 

	protected $btTable = 'btTonyProPhoto';
	protected $btInterfaceWidth = "430";
	protected $btInterfaceHeight = "450";

	protected $btCacheBlockRecord=1; 
	protected $btCacheBlockOutput=0; 
	protected $btCacheBlockOutputTimeout=600;
	protected $btCacheBlockOutputForRegisteredUsers=0;

	public $pageSize=0;
	
	
	public $maxThumbWidth=350;
	public $maxThumbHeight=80;
	public $maxWidth=600;
	public $maxHeight=400; 
	public $preload=0;
	public $watermarkText=''; //text string or fID123
	public $watermarkFontSize=9;
	public $watermarkPosition='br';
	public $watermarkAlpha=20;
	
	/*
	public function __construct(){
		
		if(!$this->maxThumbWidth) $this->maxThumbWidth=350;
		if(!$this->maxThumbHeight) $this->maxThumbHeight=80;
		if(!$this->maxWidth) $this->maxWidth=600;
		if(!$this->maxHeight) $this->maxHeight=400; 
		
	}
	*/ 
	
	
	
	/** 
	 * Used for localization. If we want to localize the name/description we have to include this
	 */
	public function getBlockTypeDescription() {
		return t("A highly customizable filmstrip style image gallery");
	}
	
	public function getBlockTypeName() {
		return t("Pro Photo");
	}
	
	public function getJavaScriptStrings() {
		return array();
	}
	
	
	public function on_page_view() { 
		$html = Loader::helper('html');
		$this->addHeaderItem( $html->javascript('jquery.ui.js') );
		$this->addHeaderItem( $html->css('jquery.ui.css') );		  
	}		
	
	public function getDisplayableFilesSets(){
		$db = Loader::db();
		$fileSetsData = $db->GetAll('select * from FileSets ORDER BY fsName ASC'); 
		$sets=array();
		foreach ($fileSetsData as $fileSetData) {
			$fs = new FileSet();
			foreach($fileSetData as $key => $value) {
				$fs->{$key} = $value;
			}
			$fsp = new Permissions($fs);
			if ($fsp->canSearchFiles()) {
				$sets[] = $fs;
			}
		}
		return $sets;
	}
	
	public function save($args) { 
		$db = Loader::db(); 
		
		$args['setIds']='';
		if( is_array($args['fsIDs']) ){
			$cleanSetIds=array();
			foreach($args['fsIDs'] as $fsID) $cleanSetIds[]=intval($fsID);
			$args['setIds']=join(',',$cleanSetIds);
		}
		
		$args['shownAttributes'] = '';
		$cleanFileAttrKeys=array();
		$cleanFileProperties=array();
		if( is_array($args['faks']) ){			
			foreach($args['faks'] as $fsID) $cleanFileAttrKeys[]=intval($fsID); 
		}
		if( is_array($args['fDefaultCols']) ){			
			foreach($args['fDefaultCols'] as $propertyName){
				if( in_array($propertyName, array('title','date','tags','description','filename','extension')) )
					$cleanFileProperties[]=$propertyName;
			}
		}
		$args['shownAttributes']=join(',', array_merge($cleanFileAttrKeys,$cleanFileProperties) );		
		
		$args['preload'] = intval($args['preload']);
		
		$args['regionHeight']= ($args['regionHeightOpt']=='fixed') ? intval($args['regionHeight']) : 0 ;
		
		
		$args['hideEmptyAttrs']=intval($args['hideEmptyAttrs']);
		
		$args['listMode'] = $args['listMode'];
				
		$args['setMode'] = ($args['setMode']=='any') ? 'any':'all';
		$args['num'] = (intval($args['num']) > 0) ? intval($args['num']) : 0;
		
		$args['displayOrderDesc'] = (intval($args['displayOrderDesc']) > 0) ? intval($args['displayOrderDesc']) : 0;		
		
		$args['orderBy'] = (strlen($args['orderBy']) > 0) ? $args['orderBy'] : 'title'; 
		
		$args['paging'] = intval($args['paging']);
		
		$args['maxWidth'] = intval($args['maxWidth']);
		$args['maxHeight'] = intval($args['maxHeight']);
		$args['maxThumbWidth'] = intval($args['maxThumbWidth']);
		$args['maxThumbHeight'] = intval($args['maxThumbHeight']);
		
		$args['detailTargetCID'] = (!intval($args['showDetailTarget']) || !intval($args['detailTargetCID']) ) ? 0 : intval($args['detailTargetCID']);
		
		$args['imgTagsTargetCID'] = (!intval($args['imgTagsTarget']) || !intval($args['imgTagsTargetCID']) ) ? 0 : intval($args['imgTagsTargetCID']);
		
		$args['watermarkThumbs']=intval($args['watermarkThumbs']);
		
		$args['watermarkFID']=intval($args['watermarkFID']);
		
		//clear the watermark cache if any of these change 
		//...unnecessary now since they have unique ids? 
		/*
		if(	$this->watermarkFID != $args['watermarkFID'] || 
			$this->watermarkThumbs != $args['watermarkThumbs'] || 
			$this->watermarkType != $args['watermarkType'] || 
			$this->watermarkText != $args['watermarkText'] || 
			$this->watermarkFontSize != $args['watermarkFontSize'] || 
			$this->watermarkAlpha != $args['watermarkAlpha'] || 
			$this->watermarkPosition != $args['watermarkPosition'] || 
			$this->watermarkThumbsText != $args['watermarkThumbsText'  ){
			$this->clearWatermarkCache(); 
		}
		*/
		
		parent::save($args);		
	}
	
	public function getFileList( $opts=array() ){
		
		$fileList = new FileList(); 
		
		if(defined('PRO_PHOTO_FILE_PERMISSIONS_DISABLED') && PRO_PHOTO_FILE_PERMISSIONS_DISABLED==true) 
			$fileList->setPermissionLevel(false); 			
		
		$fileList->filterByType(FileType::T_IMAGE);
		
		//filter by file sets		
		$setIdsArray = explode( ',', $opts['setIds'] );	
		if( $opts['setMode']=='any'){ // join/combine sets
			$fileSetJoinClauses=array();
			foreach( $setIdsArray as $setId ){ 
				$fileSet = FileSet::getByID( intval($setId) );
				if(!intval($setId) || !$fileSet) continue; 
				$tableAliasName='fsf'.intval($setId);
				$fileList->addToQuery("left join FileSetFiles {$tableAliasName} on {$tableAliasName}.fID = f.fID"); 		
				$fileSetJoinClauses[]="{$tableAliasName}.fsID=".intval($setId);
			}
			if(count($fileSetJoinClauses))
				$fileList->filter(false, '('.join(' OR ',$fileSetJoinClauses).')');
		}else{ //intersect sets
			foreach( $setIdsArray as $setId ){
				if(!intval($setId)) continue;
				$fileSet = FileSet::getByID( intval($setId) ); 
				$fileList->filterBySet($fileSet);
			}
		}	
		
		//filter by tag - right now it only filters by one
		if(strlen(trim( $opts['tags'] ))){  
			//$fileList->filter(false, "( fv.fvTags like ".$db->qstr("%\n".$this->tags."\n%")."  )");
			$fileList->filterByTag( $opts['tags'] );
		} 	
		
		//search
		if( $opts['keywords'] ){
			$fileList->filterByKeywords( $opts['keywords'] );
		}
		
		//sorting 
		$sortBy= $opts['displayOrder']; 
		$sortAscDesc = (intval($opts['displayOrder'])>0) ? 'desc' : 'asc';
		if($sortBy=='date'){
			$fileList->sortBy('fDateAdded', $sortAscDesc);
		}elseif($sortBy=='filename'){
			$fileList->sortBy('fvFilename', $sortAscDesc);
		}elseif($sortBy=='random'){
			$fileList->sortBy('rand()','');				
			$this->btCacheBlockOutputForRegisteredUsers=0;
			$this->btCacheBlockOutput=0;				
		}elseif( substr($sortBy,0,4)=='fak_' ){
			$fakID=substr($sortBy,4); 
			$fileList->sortByAttributeKey( intval($fakID), $sortAscDesc); 
		}elseif($sortBy=='custom_sequence' && $opts['displayOrderIdSequence']){
			$cleanIds=array();
			$rawIds=explode(',',$opts['displayOrderIdSequence'] );
			foreach($rawIds as $rawId){
				if(intval($rawId)) $cleanIds[]=intval(trim($rawId)); 
			}
			$cleanIds=array_reverse($cleanIds);
			$sortAscDesc=($sortAscDesc=='desc')?'asc':'desc';
			if(count($cleanIds)) 
				$fileList->sortBy('FIELD(f.fID,'.join(',',$cleanIds).')', $sortAscDesc); 
		}elseif( method_exists($fileList,'sortByFileSetDisplayOrder') ){ 
			$fileList->sortByFileSetDisplayOrder();
		}else{ // "title"
			$fileList->sortBy('fvTitle', $sortAscDesc ); 
		}			
		
		return $fileList;
	}
			
	public function view() {
		global $c;	
		$db=Loader::db();  
		
		$bID=$this->bID;
		
		if(isset($this->shownAttributes))
			$_SESSION['pro_photo_attributes']=$this->shownAttributes;
		if(isset($this->maxWidth))
			$_SESSION['pro_photo_max_width']=$this->maxWidth;	
		if(isset($this->maxHeight))
			$_SESSION['pro_photo_max_height']=$this->maxHeight;	
		if(isset($this->downloadPrevention))
			$_SESSION['downloadPrevention']=intval($this->downloadPrevention); 
		
		if( is_array($this->files) ){
			
			$files=$this->files;
			$filesCount=count($this->files);
			
		}else{
			
			$opts = array(
				'setIds' => $this->setIds,
				'setMode' => $this->setMode,
				'tags' => $this->tags,
				'keywords' => $_REQUEST['keywords'],
				'displayOrder' => $this->displayOrder,
				'sortAscDesc' => $this->displayOrderDesc,
				'displayOrderIdSequence'=> $this->displayOrderIdSequence
			);
			
			$fileList = $this->getFileList( $opts ); 	
			
			//Paging
			$defaultPageSize=(intval($this->pageSize))?intval($this->pageSize):1000;
			$pageSize=(intval($_REQUEST['pageSize'])>0)?intval($_REQUEST['pageSize']):$defaultPageSize;			
			$pageNum=(intval($_REQUEST[$bID.'_pg'])>0)?intval($_REQUEST[$bID.'_pg']):1; 		
			$filesCount = $fileList->getTotal();
			
			$pagination = Loader::helper('pagination');	 
			$pagination->queryStringPagingVariable=$bID.'_pg';			
			$page_base=$c->getCollectionPath();
			if(!strstr($page_base,'?'))$page_base.='?'; 
			$paging_url = $page_base.'&sortBy='.$sortBy.'&pageSize='.$pageSize.'&keywords='.urlencode($keywords);
			//$paging_url.= '&department='.urlencode($department).'&company='.urlencode($company);
			$pagination->init( $pageNum, $filesCount, View::url($paging_url), $pageSize );			
			$files = $fileList->get( $pageSize, $pagination->result_offset );	

		} 

		$this->set( 'fileCols', $this->getDetailCols() );	 
		$this->set('filesCount',$filesCount);						
		$this->set('files',$files);
		$this->set('sortBy', $sortBy);
		$this->set('pageSize', $pageSize);
		$this->set('paginator', $pagination);
		$this->set('new_paging_url', str_replace('%pageNum%','1',$paging_url) ); 
		$this->set('listMode',$this->listMode);	
		
		if(intval($this->detailTargetCID)){
			$linkTargetPage=Page::getById($this->detailTargetCID);
			if(is_object($linkTargetPage)){
				$linkTargetUrl=$linkTargetPage->getCollectionPath();	
				if(!strstr($linkTargetUrl,'?')) $linkTargetUrl.='?';
				$this->linkTargetUrl=$linkTargetUrl;
			}
		}		
	}
	
	public function getDetailCols(){
		//get attribute keys to display
		$attrKeysIDsArray = explode( ',', $this->shownAttributes );
		$fileCols=array();
		foreach($attrKeysIDsArray as $attrKeysID){
			//if it's an integer, we'll assume it's for the attribute key
			if( intval($attrKeysID)>0 ){
				$fileAttrKey=FileAttributeKey::get($attrKeysID);
				if(!$fileAttrKey) continue;
				$fileCols[]=$fileAttrKey;
			//otherwise, it's one of the default file properties, and we'll treat it as a string
			}elseif(strlen($attrKeysID)){
				$fileCols[]=$attrKeysID;
			}
		}
		return $fileCols; 
	}
	
	public function getMainImgHTML($fv){  
		
		$imgData = $this->watermarkImage( $fv, $this->maxWidth, $this->maxHeight );  
		
		ob_start();  ?>
        <table><tr><td class="imgBorder" ><!--
		--><?php echo $aOpen ?><img src="<?php echo  $imgData->relPath ?>" width="<?php echo $imgData->width ?>" height="<?php echo $imgData->height ?>" class="width<?php echo $imgData->width ?> height<?php echo $imgData->height ?>" alt="<?php echo str_replace('"','', $fv->getTitle()) ?>" <?php  if($this->downloadPrevention){ ?> oncontextmenu="return false"<?php  } ?> /><?php echo $aClose ?><!--
        --></td></tr></table>
		<?php  
		$html=ob_get_contents();
		ob_end_clean();
		return $html;	
	}
	


	/* ********************************************************* 
	START WATERMARK SCRIPT   */
	
	public function watermarkImage( $fv, $maxWidth=500, $maxHeight=500, $isThumbnail=0 ){
		
		$newImgData = new stdClass;
		
		$watermarkType = defined('WATERMARK_TYPE') ? WATERMARK_TYPE : $this->watermarkType;
		$watermarkThumbs = defined('WATERMARK_THUMBS') ? WATERMARK_THUMBS : $this->watermarkThumbs;
		$watermarkThumbsText = defined('WATERMARK_THUMBS_TEXT') ? WATERMARK_THUMBS_TEXT : $this->watermarkThumbsText;
		$watermarkText = defined('WATERMARK_TEXT') ? WATERMARK_TEXT : $this->watermarkText;
		
		//prepare watermark position and transparency variables 
		$position = defined('WATERMARK_POSITION') ? strtolower(WATERMARK_POSITION) : $this->watermarkPosition;
		$watermarkAlpha = defined('WATERMARK_FONT_ALPHA') ? intval(WATERMARK_FONT_ALPHA) : intval($this->watermarkAlpha); //0-127
		if($watermarkAlpha>100) $watermarkAlpha=100;
		elseif($watermarkAlpha<0) $watermarkAlpha=0;		
		
		if( $isThumbnail) 
			 $watermarkText = $watermarkThumbsText; 
		
		$sourceFile = $fv->getPath();
		
		$sizes=getimagesize( $sourceFile ); 
		
		$newImgData->resized = 0;//resize false
		$newImgData->origWidth = $sizes[0]; //orginal width 
		$newImgData->origHeight = $sizes[1]; //orginal height 
		$newImgData->width = $sizes[0]; //new width 
		$newImgData->height = $sizes[1]; //new height 
		
		//resize the image based on the max width and height
		if( $newImgData->width > $maxWidth){
			$newImgData->resized = 1;//resize true
			$newImgData->height=$maxWidth*($newImgData->height/$newImgData->width);
			$newImgData->width=$maxWidth;
		}
		if( $newImgData->height > $maxHeight){ 
			$newImgData->resized = 1; //resize true
			$newImgData->width = $maxHeight*($newImgData->width/$newImgData->height);
			$newImgData->height = $maxHeight;
		}			
		$newImgData->height = round($newImgData->height);	
		$newImgData->width = round($newImgData->width);	
		
		//get the file name from the source image path 
		$filePathParts = explode('/',str_replace( "\\", '/', $sourceFile) ); 
		if( !count($filePathParts) )
			return ''; 
		$fileName = $filePathParts[count($filePathParts)-1]; 
		
		//get the file extension
		$fileNameParts = explode('.',$fileName); 
		if( count($filePathParts)>1 ) 
			$newImgData->fileExt = $fileNameParts[count($fileNameParts)-1];
		
		//give the image to be created a unique path
		if( $watermarkType=="image" && (!$isThumbnail || $watermarkThumbs) ) 
			$watermarkID = $this->watermarkFID . ':' . $position . ':' . intval($watermarkAlpha);
		elseif( $watermarkType=="text" && trim($watermarkText) ) 
			$watermarkID = $watermarkText . ':' . $position . ':' . intval($this->watermarkFontSize) . ':' . intval($watermarkAlpha); 
		$newImgData->fileName = 'wm_'.md5($watermarkID . ':' . $sourceFile . ':' . $maxWidth . ':' . $maxHeight ).'.'.$newImgData->fileExt; 
		
		$watermarksFolder = DIR_FILES_CACHE . '/';
		
		//image relative path 
		$newImgData->relPath = REL_DIR_FILES_CACHE . '/' . $newImgData->fileName; 
		
		//image absolute path 
		$newImgData->absPath = $watermarksFolder . '/' . $newImgData->fileName; 
		
		//has the watermarked image already been created? 
		if( file_exists($newImgData->absPath) )
			return $newImgData; 
		
		//does the watermark cache folder exist? 
		if( !file_exists( $watermarksFolder ) ) 
			mkdir($watermarksFolder); 
			
		//load image into memory
		$filetype = strtolower($newImgData->fileExt);
			
		switch($sizes[2]) {
			case IMAGETYPE_GIF:
				$sourceImage = @imageCreateFromGIF($sourceFile);
				break;
			case IMAGETYPE_JPEG:
				$sourceImage = @imageCreateFromJPEG($sourceFile);
				break;
			case IMAGETYPE_PNG:
				$sourceImage = @imageCreateFromPNG($sourceFile);
				break;
		} 
		
		$newImage = imagecreatetruecolor( $newImgData->width, $newImgData->height);
		
		if( $sizes[2] == IMAGETYPE_PNG || $sizes[2] == IMAGETYPE_GIF ){ 
			imagealphablending($newImage, false); 
			$color = imagecolortransparent($newImage, imagecolorallocatealpha($sourceImage, 0, 0, 0, 127));
			imagefill($newImage, 0, 0, $color);
			imagesavealpha($newImage, true); 
		}			
		
		$res = imagecopyresampled( $newImage, $sourceImage, 0, 0, 0, 0, $newImgData->width, $newImgData->height, $newImgData->origWidth, $newImgData->origHeight );
		
		imagealphablending($newImage, true);
		
		//should we add a watermark image? 
		if( $watermarkType=="image" && (!$isThumbnail || $watermarkThumbs) ){ 
		
			$watermarkImgInfo = $this->getWatermarkObj($watermarkAlpha);
			$watermarkImage = $watermarkImgInfo->img; 
			
			
			if( $watermarkImage ){   
			
				$watermarkImageSize = $watermarkImgInfo->sizeData; 
				
				//constrain watermark width to be no greater than 75% size of containing image 
				$watermarkMaxPercent= defined('WATERMARK_IMAGE_MAX_PERCENT') ? WATERMARK_IMAGE_MAX_PERCENT : .66; 
				$origWatermarkWidth=$watermarkImageSize[0]; 
				$origWatermarkHeight=$watermarkImageSize[1]; 
				if( intval($watermarkImageSize[0]) > round($newImgData->width * $watermarkMaxPercent) ){ 
					$watermarkImageSize[1]= round($newImgData->width * $watermarkMaxPercent * ($watermarkImageSize[1] / $watermarkImageSize[0]));
					$watermarkImageSize[0]= $newImgData->width * $watermarkMaxPercent;
					$watermarkResized=1;  
				}elseif( $watermarkImageSize[1] > round( $newImgData->height * $watermarkMaxPercent ) ){ 			 
					$watermarkImageSize[0]= round($newImgData->height * $watermarkMaxPercent * ($watermarkImageSize[0] / $watermarkImageSize[1]));
					$watermarkImageSize[1]= $newImgData->height * $watermarkMaxPercent;
					$watermarkResized=1; 
				}
				
				if($position=='c'){
					$coordinateX = round($newImgData->width/2) - round($watermarkImageSize[0]/2); 
					$coordinateY = round($newImgData->height/2) - round($watermarkImageSize[1]/2); 
				}else{
					$coordinateX = ($position=='tl' || $position=='bl') ? 5 : ( $newImgData->width - 5) - intval($watermarkImageSize[0]);
 					$coordinateY = ($position=='tl' || $position=='tr') ? 5 : ( $newImgData->height - 5) - intval($watermarkImageSize[1]) ;
				}
				
				//resample image to alpha transparency 
				if( $watermarkAlpha!=100 ){ 
					
					if( !$watermarkImgInfo->containsAlphaPixels && $watermarkResized ){ 
					
						$newWatermarkImage = imagecreatetruecolor( $watermarkImageSize[0], $watermarkImageSize[1]);
						imagealphablending($newWatermarkImage, false);
						@imagecopyresampled( $newWatermarkImage, $watermarkImage, 0, 0, 0, 0, $watermarkImageSize[0], $watermarkImageSize[1], $origWatermarkWidth, $origWatermarkHeight );
						$watermarkImage=$newWatermarkImage; 
						
					}
					
				}
				
				if( !$watermarkImgInfo->containsAlphaPixels ) 
					imagecopymerge($newImage, $watermarkImage, $coordinateX, $coordinateY, 0, 0, intval($watermarkImageSize[0]), intval($watermarkImageSize[1]), $watermarkAlpha);
				else 
					imagecopyresampled($newImage, $watermarkImage, $coordinateX, $coordinateY, 0, 0, intval($watermarkImageSize[0]), intval($watermarkImageSize[1]), $origWatermarkWidth, $origWatermarkHeight );
				
			}
		
		//or watermark text 
		}elseif( $watermarkType=="text" && $watermarkText ){ 
		
			$watermarkFontSize = defined('WATERMARK_FONT_SIZE') ? intval(WATERMARK_FONT_SIZE) : intval($this->watermarkFontSize);		
			
			$watermarkAlpha = round((100-$watermarkAlpha)*127/100);
			
			$black = imagecolorallocatealpha($newImage, 0, 0, 0, round($watermarkAlpha/2) );
			$white = imagecolorallocatealpha($newImage, 255, 255, 255, $watermarkAlpha );
			
			$b = $this->getBlockObject();
			if(is_object($b)) $blockPath = $b->getBlockPath(); 
			if(!$blockPath) $blockPath= dirname(__FILE__);
			$font = $blockPath.'/images/VeraMono.ttf';  
			
			$coordinateX = ($position=='tl' || $position=='bl') ? 5 + round( $watermarkFontSize * .2 ) : ( $newImgData->width - 5) - round( strlen($watermarkText) * $watermarkFontSize * .75 );
 			$coordinateY = ($position=='tl' || $position=='tr') ? (5 + round( $watermarkFontSize * 1 ))  : ( $newImgData->height - 5) - round( $watermarkFontSize * .2 ) ;

			// Add some shadow to the text
			imagettftext($newImage, $this->watermarkFontSize, 0, $coordinateX, $coordinateY, $black, $font, $watermarkText);
			
			imagettftext($newImage, $this->watermarkFontSize, 0, $coordinateX-1, $coordinateY-1, $white, $font, $watermarkText);
		}
		
		if ($res) {
			switch($sizes[2]) {
				case IMAGETYPE_GIF:
					$res2 = imageGIF($newImage, $newImgData->absPath);
					break;
				case IMAGETYPE_JPEG:
					$compression = (defined('AL_THUMBNAIL_JPEG_COMPRESSION') && intval(AL_THUMBNAIL_JPEG_COMPRESSION)) ? intval(AL_THUMBNAIL_JPEG_COMPRESSION) : 90; 
					$res2 = imageJPEG($newImage, $newImgData->absPath, $compression);
					break;
				case IMAGETYPE_PNG:
					$res2 = imagePNG($newImage, $newImgData->absPath);
					break;
			}
		}
		
		@imagedestroy($sourceImage);
		@imagedestroy($newImage);
		
		return $newImgData;		
	}
	
	
	//this function is intended to reduce the amount of processing associated with the watermark img.  
	public function getWatermarkObj($watermarkAlpha){ 
	
		if( $this->watermarkImgData ) 
			return $this->watermarkImgData; 
	
		$imgData = new stdClass; 
			
		$watermarkFID = defined('WATERMARK_FID') && intval(WATERMARK_FID) ? intval(WATERMARK_FID) : intval($this->watermarkFID);
			
		$watermarkFile=File::getById($watermarkFID);
			
		if( ($watermarkFile) ){
				
			$watermarkfv = $watermarkFile->getApprovedVersion(); 
			
			$watermarkPath = $watermarkfv->getPath();
		
			$watermarkImageSize = @getimagesize($watermarkPath);
			
			$imgData->sizeData = $watermarkImageSize; 
					
			switch($watermarkImageSize[2]) {
				case IMAGETYPE_GIF:
					$watermarkImage = @imageCreateFromGIF($watermarkPath);
					break;
				case IMAGETYPE_JPEG:
					$watermarkImage = @imageCreateFromJPEG($watermarkPath);
					break;
				case IMAGETYPE_PNG:
					$watermarkImage = @imageCreateFromPNG($watermarkPath);
					break;
			}
			
			if( $watermarkImageSize[2] == IMAGETYPE_PNG || $watermarkImageSize[2] == IMAGETYPE_GIF ){
				imagealphablending($watermarkImage, false);
				imagesavealpha($watermarkImage, true);					
				$color = imagecolortransparent($watermarkImage, imagecolorallocatealpha($watermarkImage, 0, 0, 0, 127));
				imagefill($watermarkImage, 0, 0, $color);
			}
			
			//resample image to alpha transparency 
			if( $watermarkAlpha!=100 ){ 
			
				$imgData->containsAlphaPixels = 0; 
				for( $y=0; $y<$imgData->sizeData[1]; $y++ ){
					for( $x=0; $x<$imgData->sizeData[0]; $x++ ){ 
						$ovrARGB = imagecolorat($watermarkImage, $x, $y);
						$ovrA = ($ovrARGB >> 24) << 1;
						if( $ovrA>0 ){ 
							$imgData->containsAlphaPixels=1; 
							break; 
						}
					}
				}
				
				if( $imgData->containsAlphaPixels ){ 
				
					for( $y=0; $y<$imgData->sizeData[1]; $y++ ){
						for( $x=0; $x<$imgData->sizeData[0]; $x++ ){
							
							$ovrARGB = imagecolorat($watermarkImage, $x, $y);
							$ovrA = ($ovrARGB >> 24) << 1;
							$ovrR = $ovrARGB >> 16 & 0xFF;
							$ovrG = $ovrARGB >> 8 & 0xFF;
							$ovrB = $ovrARGB & 0xFF;
							
							$ovrA = 127- round( (255-$ovrA)/255 * ($watermarkAlpha/100) * 127 );
							
							$dstRGB = imagecolorallocatealpha($watermarkImage, $ovrR, $ovrG, $ovrB, $ovrA ); 
							imagesetpixel($watermarkImage, $x, $y, $dstRGB);
						}
					}
					
				}
				
			}
			
			$imgData->img = $watermarkImage; 
		
		}
		
		$this->watermarkImgData = $imgData; 
		
		return $imgData; 
	}
	
	
	public function clearWatermarkCache(){ 
		$watermarksFolder = DIR_FILES_CACHE . '/';
		$mydir = opendir($watermarksFolder);
		while(false !== ($file = readdir($mydir))) {
			if($file != "." && $file != ".." && !is_dir($watermarksFolder.$file) && strpos($file,'wm_')==0 ) { 
				@chmod($watermarksFolder.$file, 0777);
				@unlink($watermarksFolder.$file);
			}
		}
		closedir($mydir);
	}
	
	/* END WATERMARK SCRIPT 
	********************************************************* */

	
	
	public function getDetailsHTML($fv,$imgTagsTargetCID=NULL) {
		if( $imgTagsTargetCID ) $this->imgTagsTargetCID=intval($imgTagsTargetCID);
		$currentFile=$fv->getFile();
		$fileCols=$this->getDetailCols();
		ob_start();
		if( in_array('description',$fileCols) ){ 
			$desc=$fv->getDescription();
			if( $this->hideEmptyAttrs || strlen($desc) ){ ?>
				<div class="proPhoto_imgDetail">  
					<span class="description">
						<?php echo ( strlen($desc) != strlen(strip_tags($desc)) ) ? $desc : htmlentities($desc) ?>
					</span>
				</div>
		<?php 	}  
		} ?> 
	
		<?php  if( in_array('tags',$fileCols) ){ 
			$tags = $fv->getTagsList();
			if( !$this->hideEmptyAttrs || count($tags) ){ 
				if(intval($this->imgTagsTargetCID)){
					$tagsLinkPage=Page::getById($this->imgTagsTargetCID);
					if(is_object($tagsLinkPage)){
						$nh = Loader::helper('navigation');
						$tagsLinkURL = $nh->getLinkToCollection($tagsLinkPage); 
					}
				}
				?>
				<div class="proPhoto_imgDetail"> 
					<span class="proPhoto_detailLabel"><?php echo t('Tags') ?>:</span>
					<span class="tagLinks">
						<?php  foreach($tags as $tag){ 
							if($notFirst) echo ', ';
							if( strlen($tagsLinkURL) ){ 
							?>							
							<a href="<?php echo $tagsLinkURL.'?tag='.$tag ?>"><?php echo htmlentities($tag) ?></a><!--
						 --><?php   
							}else echo htmlentities($tag);
							$notFirst=1;
						} ?>
					</span>
				</div>
		<?php  	} 
		} ?> 
		
		<?php   
		//loop through all attribute columns		
		foreach($fileCols as $fileCol){ 	
			if( is_object($fileCol) && get_class($fileCol)=='FileAttributeKey' ){ 
				if( !$this->hideEmptyAttrs || strlen($fv->getAttribute( $fileCol->getAttributeKeyHandle(), false )) ){  ?>
					<div class="proPhoto_imgDetail"> 
						<span class="proPhoto_detailLabel"><?php echo  htmlentities($fileCol->getAttributeKeyName()) ?>: </span>
						<?php  
						if( $fileCol->getAttributeKeyType()=='SELECT_MULTIPLE' ) echo str_replace(array("\r","\n"),', ',trim(htmlentities($fv->getAttribute( $fileCol->getAttributeKeyHandle(), false ))));
						else echo htmlentities($fv->getAttribute( $fileCol->getAttributeKeyHandle(), true ));  							
						?>
					</div>
			<?php  	}
			}elseif($fileCol=='date'){ ?>
				<div class="proPhoto_imgDetail">
					<span class="proPhoto_detailLabel"><?php echo t('Date Added')?>: </span>
					 <?php echo date('M d, Y g:ia', strtotime($currentFile->getDateAdded()))?> 
				</div>
			<?php  } ?>
		<?php  }
		$html=ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	
	public $linkAttributeHandle='proPhoto_linkURL';
	function getImageLink($fileVersion,$noSelfLink=0,$c=NULL){ 
		if( $this->enlarge_mode=='none' ) return ''; 
		$link = $fileVersion->getAttribute($this->linkAttributeHandle);
		$nh = Loader::helper('navigation');  
		if( intval($link) ){ 
			$linkPage = Page::getById(intval($link));
			if( is_object($linkPage) ) $link = $nh->getLinkToCollection($linkPage); 
		}
		if(strlen($link)) return $link;
		if(!$this->linkTargetUrl){ 
			if(!$c) $c=$this->getCollectionObject();
			if( $noSelfLink ){
				return '';
			}else{   
				$link = $nh->getLinkToCollection($c);
				if(!strstr($link,'?')) $link.='?';
				$link.='&pp_fID_'.intval($this->bID).'='.$fileVersion->getFileID();
				return $link;
			}
		}
		return View::url($this->linkTargetUrl).'fID='.$fileVersion->getFileID(); 
	}
	
}

?>