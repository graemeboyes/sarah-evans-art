<?php   
defined('C5_EXECUTE') or die(_("Access Denied.")); 

$uh = Loader::helper('concrete/urls');
$fh = Loader::helper('form/color'); 
Loader::model("collection_types");
$al = Loader::helper('concrete/asset_library');

$fileAttributes = FileAttributeKey::getList(); 
$attrColsArray = explode( ',', $controller->shownAttributes );

$setIdsArray = explode( ',', $controller->setIds );
$fileSets=$controller->getDisplayableFilesSets();

$fileList = new FileList();
?> 

<script>
proPhotoFormHelper.noImagesDisplayedError="<?php echo  t('No image interface be displayed with these options') ?>";
</script>

<style> 
.ccm-blockEditPane { margin-top:12px; }

#proPhoto_sortableImages { list-style:none; margin:0; padding:0; }
#proPhoto_sortableImages li { float:left; padding:4px; cursor:move; }
#proPhoto_sortableImagesTitle { margin-bottom:4px; }
#proPhoto_sortableImages var { display:none; }

.ccm-pagelistPane .ccm-note { padding-top:4px; }
#ccm-block-field-group-watermark-alpha .ccm-note { display:inline; }
</style>

<div id="ccm-pagelistPane-add" class="ccm-pagelistPane"> 
    
    
    <ul id="ccm-blockEditPane-tabs" class="ccm-dialog-tabs">
        <li class="ccm-nav-active"><a id="ccm-blockEditPane-tab-images" href="javascript:void(0);"><?php   echo t('Images') ?></a></li>
        <li class=""><a id="ccm-blockEditPane-tab-interface"  href="javascript:void(0);"><?php   echo t('Interface')?></a></li> 
        <li class=""><a id="ccm-blockEditPane-tab-design"  href="javascript:void(0);"><?php   echo t('Design')?></a></li> 
        <li class=""><a id="ccm-blockEditPane-tab-watermark"  href="javascript:void(0);"><?php   echo t('Watermark')?></a></li> 
    </ul>

   	<div id="ccm-blockEditPane-images" class="ccm-blockEditPane">
	
		<input id="proPhoto_servicesURL" name="servicesURL" type="hidden" value="<?php echo View::url('/tools/packages/tony_pro_photo/services/')?>" />
		
		<input id="proPhoto_sortTitle" name="sortTitle" type="hidden" value="<?php echo t('Image Display Order') ?>" />
     
        <div class="ccm-block-field-group">
          <h2><?php  echo t('File Sets (optional)')?></h2>
          <?php  if( !count($fileSets) ){ 
            echo '<div>'.t("No file sets found").'</div>';
          }else foreach($fileSets as $fileSet){ ?>
            <div>
                <input name="fsIDs[]" type="checkbox" value="<?php echo $fileSet->getFileSetID() ?>" <?php echo (in_array($fileSet->getFileSetID(),$setIdsArray))?'checked':'' ?> />
                <?php echo $fileSet->getFileSetName() ?>		
            </div>
          <?php  } ?>
          <div class="ccm-note" style="margin-top:4px"><?php echo  t('Use the %sfile manager%s to place images into sets','<a href="'.View::url('/dashboard/files').'">','</a>') ?></div> 
        </div>
        
        <div class="ccm-block-field-group">
          <h2><?php  echo t('Files must be...')?></h2> 
          <input name="setMode" type="radio" value="all" <?php echo ($controller->setMode!='any')?'checked':''?> /> <?php echo  t('in all sets') ?> &nbsp;&nbsp;   
          <input name="setMode" type="radio" value="any" <?php echo ($controller->setMode=='any')?'checked':''?> /> <?php echo  t('in any set') ?>
        </div>	 
        
        <div class="ccm-block-field-group">
          <h2><?php  echo t('Filter By Tag (optional)')?></h2>  
          <input id="proPhoto_edit_tags" name="tags" type="text" value="<?php echo htmlentities($controller->tags, ENT_QUOTES, 'UTF-8') ?>" />
        </div>	
            
        <div class="ccm-block-field-group">
            <h2><?php  echo t('Order By')?></h2>  
            <?php  
            $orderBy=$controller->displayOrder;
            ?>
            <select name="displayOrder" onchange="proPhotoFormHelper.showCustomOrder(this)">
                <option value="title" <?php echo ($orderBy=='title')?'selected':''?> ><?php echo  t('Title') ?></option>
                <option value="date" <?php echo ($orderBy=='date')?'selected':''?> ><?php echo  t('Date Posted') ?></option>
                <option value="filename" <?php echo ($orderBy=='filename')?'selected':''?> ><?php echo  t('File Name') ?> </option>
                <option value="random" <?php echo ($orderBy=='random')?'selected':''?> ><?php echo t('Random') ?></option>
                
                <?php  if( method_exists($fileList,'sortByFileSetDisplayOrder') ){ ?> 
                <option value="set" <?php echo ($orderBy=='set')?'selected':''?> ><?php echo t('Set Order') ?></option>
                <?php  } ?>
                
                <?php  foreach($fileAttributes as $ak){ 
					if($ak->getAttributeKeyHandle()=='tag') continue;
                    $akID=$ak->getAttributeKeyID();
                    ?>
                    <option value="fak_<?php echo $akID ?>"
                        <?php echo  ($orderBy=='fak_'.$akID) ? 'selected':''?>
                        >
                        <?php echo  $ak->getAttributeKeyName() ?>
                    </option>
                <?php  } ?>
                <option value="custom_sequence" <?php echo ($orderBy=='custom_sequence')?'selected':''?> ><?php echo  t('Custom Sequence') ?></option>
            </select>
              
            <input name="displayOrderDesc" type="checkbox" value="1" <?php echo (intval($controller->displayOrderDesc)>0)?'checked':'' ?> /> <?php echo  t('Descending') ?>   	
            
            <div id="ccm-custom-display-sequence-wrap" style="margin-top:8px;display:<?php echo ($orderBy=='custom_sequence')?'block':'none'?>">
                <input id="displayOrderIdSequence" name="displayOrderIdSequence" type="hidden" value="<?php echo addslashes($controller->displayOrderIdSequence) ?>" style="width:150px;" />
				<a id="proPhoto_imageOrderPopup" href="#"><?php echo t('Set Image Order') ?></a>
            </div>
        </div> 
        
        <div class="ccm-block-field-group">
          <h2><?php  echo t('Page Size')?></h2> 
          <input name="pageSize" type="text" value="<?php echo intval($controller->pageSize) ?>" style="width:50px;" />
          <input name="paging" type="checkbox" value="1" <?php echo intval($controller->paging)?'checked="checked"':'' ?> /> <?php echo t('Show paging links')?>
          <div class="ccm-note" style="margin-top:4px"><?php echo t('Enter zero for no page size limit') ?></div>
        </div> 
		
		<div class="ccm-block-field-group">
			
			<input name="preload" type="checkbox" value="1" <?php echo ($controller->preload)?'checked="checked"':'' ?> /> 
			<?php  echo t("Preload large images")?>
		</div>	  
        
    </div>    
	
    
        
        
        
    <div id="ccm-blockEditPane-interface" class="ccm-blockEditPane" style="display:none">    
        
        <div class="ccm-block-field-group">
          <h2><?php  echo t('Thumbs display')?></h2> 
          <input name="listMode" type="radio" value="filmstrip" <?php echo ($controller->listMode!='grid' && $controller->listMode!='none')?'checked':''?> /> <?php echo t('Film Strip') ?>&nbsp;&nbsp;    
          <input name="listMode" type="radio" value="grid" <?php echo ($controller->listMode=='grid')?'checked':''?> /> <?php echo t('Grid') ?>&nbsp;&nbsp;   
          <input name="listMode" type="radio" value="none" <?php echo ($controller->listMode=='none')?'checked':''?> /> <?php echo t('None') ?>
        </div>
        
        
        <div class="ccm-block-field-group" id="proPhoto_maxThumbSizeWrap" style="display:<?php echo ($controller->listMode!='none')?'block':'none'?>">
          <h2><?php  echo t('Max Thumbnail Size')?></h2>  
          <?php echo t('Width:')?><input name="maxThumbWidth" type="text" value="<?php echo intval($controller->maxThumbWidth) ?>" size="4" /><?php echo t('px')?>&nbsp; 
          <?php echo t('Height:')?><input name="maxThumbHeight" type="text" value="<?php echo intval($controller->maxThumbHeight) ?>" size="4" /><?php echo t('px')?>&nbsp;  
        </div>    
        
        <div class="ccm-block-field-group">
          <h2><?php  echo t('Enlarge Mode')?></h2> 
          <input name="enlarge_mode" type="radio" value="large_image" <?php echo ($controller->enlarge_mode!='link' && $controller->enlarge_mode!='none')?'checked':''?> /> <?php echo t('Large Image') ?>&nbsp;&nbsp;   
          <input name="enlarge_mode" type="radio" value="link" <?php echo ($controller->enlarge_mode=='link')?'checked':''?> /> <?php echo  t('Page Link') ?>&nbsp;&nbsp; 
          <input name="enlarge_mode" type="radio" value="none" <?php echo ($controller->enlarge_mode=='none')?'checked':''?> /> <?php echo  t('None') ?>&nbsp;&nbsp;   
        </div>	
        
        <div id="proPhoto_maxSizeWrap" style="display:<?php echo ($controller->enlarge_mode!='link' && $controller->enlarge_mode!='none')?'block':'none'?>">
		
			<div class="ccm-block-field-group">
			  <h2><?php  echo t('Max Enlarged Image Size')?></h2>  
			  <?php echo t('Width:')?><input name="maxWidth" type="text" value="<?php echo intval($controller->maxWidth) ?>" size="4" /><?php echo t('px')?>&nbsp; 
			  <?php echo t('Height:')?><input name="maxHeight" type="text" value="<?php echo intval($controller->maxHeight) ?>" size="4" /><?php echo t('px')?>&nbsp; 
			</div>     
			
			<div class="ccm-block-field-group">
			  <h2><?php  echo t('Large Image Region Height')?></h2>  
			  <input name="regionHeightOpt" type="radio" value="auto" <?php echo (!intval($controller->regionHeight))?'checked':''?> /> <?php echo t('Auto') ?> &nbsp;&nbsp;   
			  <input name="regionHeightOpt" type="radio" value="fixed" <?php echo (intval($controller->regionHeight))?'checked':''?> /> <?php echo t('Fixed') ?> 
			  <span id="ccm-prophoto-regionHeight-wrap" style="display:<?php echo  (intval($controller->regionHeight)) ? 'inline' : 'none' ?>" >
				 <input name="regionHeight" type="text" value="<?php echo intval($controller->regionHeight) ?>" size="4" /><?php echo t('px')?> 
			  </span>
			</div> 
			
		</div> 		 
        
        
        <div id="proPhoto_imgDetailsWrap" style="display:<?php echo ($controller->enlarge_mode!='link' && $controller->enlarge_mode!='none')?'block':'none'?>">
            <div  class="ccm-block-field-group"
              <h2><?php echo  t('Image Details to Display')?></h2>  
              <div>
                <input name="fDefaultCols[]" type="checkbox" value="title" <?php echo (in_array('title',$attrColsArray))?'checked':'' ?> /> 			
                <?php echo t("Title") ?>
              </div>  
              <div>
                <input name="fDefaultCols[]" type="checkbox" value="description" <?php echo (in_array('description',$attrColsArray))?'checked':'' ?> /> 			
                <?php echo t("Description") ?>
              </div>
              <div>
                <input name="fDefaultCols[]" type="checkbox" onchange="proPhotoFormHelper.showTagsTargetWrap(this)" value="tags" <?php echo (in_array('tags',$attrColsArray))?'checked':'' ?> /> 			
                <?php echo t("Tags") ?>
              </div>	
              <div>
                <input name="fDefaultCols[]" type="checkbox" value="filename" <?php echo (in_array('filename',$attrColsArray))?'checked':'' ?> /> 			
                <?php echo t("File Name") ?>
              </div>	    	  
              <?php  foreach($fileAttributes as $ak){ 
                if( $ak->getAttributeKeyHandle()=='tag' ) continue;
                ?>
                <div>
                    <input name="faks[]" type="checkbox" value="<?php echo $ak->getAttributeKeyID() ?>" <?php echo (in_array($ak->getAttributeKeyID(),$attrColsArray))?'checked':'' ?> /> 			
                    <?php echo  $ak->getAttributeKeyName() ?>  
                </div>
              <?php  } ?>
              <div>
                <input name="fDefaultCols[]" type="checkbox" value="date" <?php echo (in_array('date',$attrColsArray))?'checked':'' ?> /> 			
                <?php echo t("Date Posted") ?>
              </div>	  
            </div>	
			
			<div class="ccm-block-field-group">
				<input name="hideEmptyAttrs" type="checkbox" value="1" <?php echo ($controller->hideEmptyAttrs)?'checked="checked"':'' ?> /> 
				<?php  echo t("Hide details that haven't been set")?>
			</div>			
        
            <div class="ccm-block-field-group" id="ccm-proPhoto-imageTagsTargetWrap" style="display:<?php echo ( in_array('tags',$attrColsArray) )?'block':'none';?>">
                <h2><?php  echo t('Image Tags Link Page')?></h2> 
                <input id="imgTagsTarget" onchange="proPhotoFormHelper.showTagsTarget(this);" name="imgTagsTarget" type="checkbox" value="1" <?php echo (intval($controller->imgTagsTargetCID))?'checked':'' ?> /> 
                <?php echo t('Link image tags to another page on your site') ?>
                
                <div id="ccm-prophoto-tags-page-target-wrap" <?php  if (!intval($controller->imgTagsTargetCID)){ ?> style="display: none"<?php   } ?>>
                    <?php   
                    $formPgSelector = Loader::helper('form/page_selector');
                    echo $formPgSelector->selectPage('imgTagsTargetCID', $controller->imgTagsTargetCID, 'proPhotoFormHelper.setPageTarget');
                    ?>
                    <div class="ccm-note"><?php echo t("The page must handle a \"tag\" parameter in the url's querystring")?></div>
                </div>
            </div>        
        </div> 
        
        <div class="ccm-block-field-group" id="ccm-proPhoto-imageTargetWrap" style="display:<?php echo ($controller->enlarge_mode!='none')?'block':'none';?>">
            <h2><?php  echo t('Image Detail Page')?></h2> 
            <input id="showDetailTarget" onchange="proPhotoFormHelper.showDetailTarget(this);" name="showDetailTarget" type="checkbox" value="1" <?php echo (intval($controller->detailTargetCID))?'checked':'' ?> /> 
            <?php echo t('Link images to another page on your site') ?>
            
            <div id="ccm-prophoto-detail-page-target-wrap" <?php  if (!intval($controller->detailTargetCID)){ ?> style="display: none"<?php   } ?>>
                <?php   
                $formPgSelector = Loader::helper('form/page_selector');
                echo $formPgSelector->selectPage('detailTargetCID', $controller->detailTargetCID, 'proPhotoFormHelper.setPageTarget');
                ?>
                <div class="ccm-note"><?php echo t("The page must handle a \"fID\" parameter in the url's querystring")?></div>
            </div>
        </div>
    
    </div>
    
    <div id="ccm-blockEditPane-design" class="ccm-blockEditPane" style="display:none">
        
        <div class="ccm-block-field-group">
          <h2><?php  echo t('Style')?></h2> 
          <select name="skin">
            <option value="gray" <?php echo ($controller->skin=='gray')?'selected="selected"':'' ?>><?php echo t('Transparent Gray') ?></option>
            <option value="embossed" <?php echo ($controller->skin=='embossed')?'selected="selected"':''?>><?php echo t('Dark Embossed') ?></option>
            <option value="white" <?php echo ($controller->skin=='white')?'selected="selected"':''?>><?php echo t('Light Gray') ?></option>
            <option value="black" <?php echo ($controller->skin=='black')?'selected="selected"':''?>><?php echo t('Ebony') ?></option>
          </select> 
        </div>
        
        <div class="ccm-block-field-group">
          <h2><?php  echo t('Selected Image Color')?></h2> 
          <?php   echo $fh->output( 'selectedImgColor', '', $controller->selectedImgColor ) ?>
          <div class="ccm-spacer"></div> 
        </div>          
        
		<div class="ccm-block-field-group">
			<h2><?php  echo t('Custom Button Images (Optional)')?></h2> 
				<?php 
				if( intval($controller->buttonLeftFID) )
					$leftButtonFile=File::getById($controller->buttonLeftFID);
				?>
				<?php echo $al->image('ccm-b-image-left', 'buttonLeftFID', t('Left'), $leftButtonFile);?>
				<?php 
				if( intval($controller->buttonRightFID) ) 
					$rightButtonFile=File::getById($controller->buttonRightFID);
				?>
				<?php echo $al->image('ccm-b-image-right', 'buttonRightFID', t('Right'), $rightButtonFile);?>         
		 </div>
		 
		<div class="ccm-block-field-group">
			<h2><?php  echo t('Custom Button Hover Images (Optional)')?></h2> 
				
				<?php 
				if( intval($controller->buttonLeftHoverFID) )
					$leftButtonHoverFile=File::getById($controller->buttonLeftHoverFID);
				?>
				<?php echo $al->image('ccm-b-image-hover-left', 'buttonLeftHoverFID', t('Left'), $leftButtonHoverFile);?>
				<?php 
				if( intval($controller->buttonRightHoverFID) )
					$rightButtonHoverFile=File::getById($controller->buttonRightHoverFID);
				?>
				<?php echo $al->image('ccm-b-image-hover-right', 'buttonRightHoverFID', t('Right'), $rightButtonHoverFile);?> 
				
				<div></div>        
		 </div>		 
            
	</div>
    
    
	<div id="ccm-blockEditPane-watermark" class="ccm-blockEditPane" style="display:none;">
    
		<div class="ccm-block-field-group">
			<h2><?php  echo t('Watermark Type')?></h2> 
        	<input name="watermarkType" type="radio" value="none" <?php echo (!$controller->watermarkType || $controller->watermarkType=='none' )?'checked':''?> /> <?php echo t('None') ?>&nbsp; 
            <input name="watermarkType" type="radio" value="text" <?php echo ($controller->watermarkType=='text')?'checked':''?> /> <?php echo t('Text') ?>&nbsp;  
        	<input name="watermarkType" type="radio" value="image" <?php echo ($controller->watermarkType=='image')?'checked':''?> /> <?php echo t('Image') ?>
		 </div>
         
         <div id="proPhoto_watermarkOptions" style="display:<?php echo ($controller->watermarkType && $controller->watermarkType!='none')?'block':'none' ?>" >
         
            <div id="proPhoto_watermarkTypeImgWrap" style="display:<?php echo ($controller->watermarkType=='image')?'block':'none' ?>">
        
                <div class="ccm-block-field-group">
                    <h2><?php  echo t('Choose Watermark Image')?></h2> 
                        
                    <?php 
                    if( intval($controller->watermarkFID) )
                        $watermarkFile=File::getById($controller->watermarkFID);
                    ?>
                    <?php echo $al->image('ccm-b-image-watermarkFID', 'watermarkFID', t('Select Image'), $watermarkFile);?>
                    <div class="ccm-note"><?php echo t('For maximum compatibility, use a PNG-8 image.') ?></div>    
                </div>
                
                <div class="ccm-block-field-group">
                  <h2><?php  echo t('Watermark Thumbnails?')?></h2> 
                  <input name="watermarkThumbs" type="checkbox" value="1" <?php echo ($controller->watermarkThumbs) ? 'checked' : '' ?> /> <?php echo t('Yes') ?>
                </div> 
            
            </div> 
        
            <div id="proPhoto_watermarkTypeTextWrap" style="display:<?php echo ($controller->watermarkType=='text')?'block':'none' ?>">
            
                <div class="ccm-block-field-group">
                  <h2><?php  echo t('Watermark Text')?></h2> 
                  <input name="watermarkText" type="text" style="width:95%" value="<?php echo htmlentities($controller->watermarkText, ENT_QUOTES, 'UTF-8') ?>" />
                  <div class="ccm-note"><?php echo t('Example: &copy; MyCompanyName.com All Rights Reserved') ?></div>
                </div> 
                
                <div class="ccm-block-field-group">
                  <h2><?php  echo t('Font Size')?></h2> 
                  <input name="watermarkFontSize" type="text" size="3" value="<?php echo intval($controller->watermarkFontSize) ? intval($controller->watermarkFontSize) : 9 ?>" />
                </div>           
                
                <div id="proPhoto_watermarkThumbnailText" class="ccm-block-field-group">
                  <h2><?php  echo t('Watermarked Thumbnail Text')?></h2> 
                  <input name="watermarkThumbsText" type="text" style="width:95%" value="<?php echo htmlentities($controller->watermarkThumbsText, ENT_QUOTES, 'UTF-8') ?>" />
                </div> 
                
            </div>
            
            <div class="ccm-block-field-group">
              <h2><?php  echo t('Watermark Position on Image')?></h2> 
              <input name="watermarkPosition" type="radio" value="tl" <?php echo ($controller->watermarkPosition=='tl' )?'checked':''?> /> <?php echo t('Top Left') ?><br />
              <input name="watermarkPosition" type="radio" value="tr" <?php echo ($controller->watermarkPosition=='tr')?'checked':''?> /> <?php echo t('Top Right') ?><br />
              <input name="watermarkPosition" type="radio" value="bl" <?php echo ($controller->watermarkPosition=='bl')?'checked':''?> /> <?php echo t('Bottom Left') ?><br />
              <input name="watermarkPosition" type="radio" value="br" <?php echo ($controller->watermarkPosition=='br' || !$controller->watermarkPosition)?'checked':''?> /> <?php echo t('Bottom Right') ?>
              <div id="proPhoto_watermarkPositionCenter" style="display:<?php echo ($controller->watermarkType!='text')?'block':'none' ?>">
              	<input name="watermarkPosition" type="radio" value="c" <?php echo ($controller->watermarkPosition=='c' && $controller->watermarkType!='text')?'checked':'' ?> /> <?php echo t('Centered') ?> 
              </div>
            </div> 
            
            <div class="ccm-block-field-group">
              <h2><?php  echo t('Transparency')?></h2> 
              <input name="watermarkAlpha" type="text" size="3" value="<?php echo  intval($controller->watermarkAlpha) ?>" />&nbsp;<?php echo t('%') ?>
            </div> 
        
        </div>
                      

	</div> 

</div> 