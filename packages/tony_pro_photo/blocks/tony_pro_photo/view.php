<?php  
defined('C5_EXECUTE') or die(_("Access Denied.")); 

//$phpthumbHelper = Loader::helper("phpthumb",'phpthumb'); 
$ih = Loader::helper('image'); 

global $c;
$documentItemLabel=$c->getCollectionAttributeValue('document_library_item_label'); 
if(!$documentItemLabel) $documentItemLabel='images';
 
$defaultValsController = new TonyProPhotoBlockController();
if(!intval($maxThumbHeight)) $maxThumbHeight= ($controller->maxThumbHeight) ? $controller->maxThumbHeight : $defaultValsController->maxThumbHeight;
if(!intval($maxThumbWidth)) $maxThumbWidth= ($controller->maxThumbWidth) ? $controller->maxThumbWidth : $defaultValsController->maxThumbWidth;
//sux, but right now you can't set the max image height hard coded style, cause it looses it when it call a new large image. 
if(!intval($maxHeight)) $maxHeight= $defaultValsController->maxHeight;
if(!intval($maxWidth)) $maxWidth= $defaultValsController->maxWidth;

if(!$controller) $controller=$defaultValsController;

if(!strlen($skin)) $proPhotoSkin='proPhotoSkin_embossed';
else $proPhotoSkin='proPhotoSkin_'.$skin;
?>

<style type="text/css">
<?php  if( strlen($selectedImgColor) ){ ?>
#proPhotoWrap<?php echo intval($bID)?> .proPhotoImgWrap.selected img, #proPhotoWrap<?php echo intval($bID)?> .proPhotoImgWrap a:hover img { border:1px solid <?php echo  $selectedImgColor ?>}
<?php  } ?>
<?php  if(intval($buttonLeftFID)){ 
	 $leftButtonFile=File::getById($buttonLeftFID);
	 if(is_object($leftButtonFile)){  
		$sizes=@getimagesize( $leftButtonFile->getPath() ); 
	 	?>
#proPhotoWrap<?php echo intval($bID)?> .mainImage table .arrowLeft { 
	background: url(<?php echo $leftButtonFile->getRelativePath() ?>) no-repeat center; 
	<?php  if(intval($sizes[0]) && intval($sizes[1])){ ?>width:<?php echo $sizes[0]?>px; height:<?php echo $sizes[1]?>px;<?php  } ?>
	opacity:1; filter: alpha(opacity=100);
	border:0px none;
}
#proPhotoWrap<?php echo intval($bID)?> .mainImage table .arrowLeft .arrowLeftInner { background:none; width:auto; height:auto; background-image:none; border:0px none !important; }
<?php  	 }
} ?>

<?php  if(intval($buttonRightFID)){ 
	 $rightButtonFile=File::getById($buttonRightFID);
	 if(is_object($rightButtonFile)){  
		$sizes=@getimagesize( $rightButtonFile->getPath() ); 
	 	?>
#proPhotoWrap<?php echo intval($bID)?> .mainImage table .arrowRight { 
	background: url(<?php echo $rightButtonFile->getRelativePath() ?>) no-repeat center; 
	<?php  if(intval($sizes[0]) && intval($sizes[1])){ ?>width:<?php echo $sizes[0]?>px; height:<?php echo $sizes[1]?>px;<?php  } ?>
	opacity:1; filter: alpha(opacity=100);
	border:0px none;
}
#proPhotoWrap<?php echo intval($bID)?> .mainImage table .arrowRight .arrowRightInner { background:none; width:auto; height:auto; background-image:none; border:0px none !important; }
<?php  	 }
} ?>

<?php  if(intval($buttonLeftHoverFID)){ 
	 $leftHoverButtonFile=File::getById($buttonLeftHoverFID);
	 if(is_object($leftHoverButtonFile)){  
		$sizes=@getimagesize( $leftHoverButtonFile->getPath() ); 
	 	?>
#proPhotoWrap<?php echo intval($bID)?> .mainImage table a:hover .arrowLeft { 
	background: url(<?php echo $leftHoverButtonFile->getRelativePath() ?>) no-repeat center; 
	<?php  if(intval($sizes[0]) && intval($sizes[1])){ ?>width:<?php echo $sizes[0]?>px; height:<?php echo $sizes[1]?>px;<?php  } ?>
} 

#proPhotoWrap<?php echo intval($bID)?> .proPhoto_imageHoverLeft { background:url(<?php echo $leftHoverButtonFile->getRelativePath() ?>) no-repeat center; }
<?php  	 }
} ?>

<?php  if(intval($buttonRightHoverFID)){ 
	 $rightHoverButtonFile=File::getById($buttonRightHoverFID);
	 if(is_object($rightButtonFile)){  
		$sizes=@getimagesize( $rightHoverButtonFile->getPath() ); 
	 	?>
#proPhotoWrap<?php echo intval($bID)?> .mainImage table a:hover .arrowRight { 
	background: url(<?php echo $rightHoverButtonFile->getRelativePath() ?>) no-repeat center; 
	<?php  if(intval($sizes[0]) && intval($sizes[1])){ ?>width:<?php echo $sizes[0]?>px; height:<?php echo $sizes[1]?>px;<?php  } ?>
} 
#proPhotoWrap<?php echo intval($bID)?> .proPhoto_imageHoverRight { background:url(<?php echo $rightHoverButtonFile->getRelativePath() ?>) no-repeat center; }
<?php  	 }
} ?>

<?php  if(intval($controller->regionHeight)>0){ ?>
#proPhotoWrap<?php echo intval($bID)?> .mainImage { height:<?php echo intval($controller->regionHeight) ?>px; } 
<?php  } ?>
</style>

	
<a name="proPhoto<?php echo intval($bID)?>" id="proPhoto<?php echo intval($bID)?>"></a>


<div id="proPhotoWrap<?php echo intval($bID)?>" class="proPhotoWrap <?php echo $proPhotoSkin ?>"> 

	<input id="proPhoto_servicesURL" name="servicesURL" type="hidden" value="<?php echo View::url('/tools/packages/tony_pro_photo/services/')?>" />
	<input id="proPhoto_viewImgEventName" name="viewImgEventName" type="hidden" value="<?php echo t('View Filmstrip Image:') ?>" />
	<input id="proPhoto_tagsCID" name="tagsCID" type="hidden" value="<?php echo intval($controller->imgTagsTargetCID); ?>" />
	<input id="proPhoto_fixedHeight" name="fixedHeight" type="hidden" value="<?php echo ( intval($controller->regionHeight)>0 )?1:0 ?>" />
	<input id="proPhoto_enlargeMode" name="enlargeMode" type="hidden" value="<?php echo  $controller->enlarge_mode ?>" />
	<input id="proPhoto_initialized" name="initialized" type="hidden" value="0" />

	<?php   
	if( !count($files) ){ ?>
	
		<div style="text-align:center; padding:24px; font-weight:bold"> 
			No <?php echo ucfirst($documentItemLabel) ?><!--
			--><?php echo (strrpos(strtolower($documentItemLabel),'s') == (strlen($documentItemLabel)-1))?'':'s'?> Found
		</div>	
									
	<?php  }else{ 
		/*
		if(isset($_REQUEST['img_num'])){
			$selectedImgNum=intval($_REQUEST['img_num']); 
			if(($selectedImgNum+1)>count($files)) $selectedImgNum=0;
		}else{
		*/
			$i=0;
			foreach($files as $file){
				$fID=$file->getFileID();
				if($fID==intval($_REQUEST[ 'pp_fID_'.intval($bID) ])){
					$selectedImgNum=$i;
					break;
				}
				$i++;
			} 
			if(($selectedImgNum+1)>count($files) || !intval($selectedImgNum)) $selectedImgNum=0; 
		?>

		<input id="proPhotoCurrentFID<?php echo intval($bID)?>" name="proPhotoCurrentFID" type="hidden" value="<?php echo $files[$selectedImgNum]->getFileID()?>" />
		<input id="proPhotoSelectedImgNum<?php echo intval($bID)?>" name="proPhotoSelectedImgNum" type="hidden" value="<?php echo $selectedImgNum ?>" />

		<?php  if( $enlarge_mode!='link' && $enlarge_mode!='none' ){ ?>
		<div class="mainImage">
		
			<div class="proPhoto_image_preloads">
				<div class="proPhoto_imageHoverLeft"></div>
				<div class="proPhoto_imageHoverRight"></div>
			</div>
			
			<table class="buttonsTable"><tr>
				<td class="arrowLeftCell">
					<?php 
					$previousImgNum = (($selectedImgNum-1)>=0) ? $selectedImgNum-1 : (count($files)-1); 
					$previousImgFile=$files[$previousImgNum];
					?>
					<a href="<?php echo View::url($c->getCollectionPath().'?pp_fID_'.$bID.'='.intval($previousImgFile->getFileID()).'&img_num='.$previousImgNum)?>"><div class="arrowLeft"><div class="arrowLeftInner"></div></div></a>
				</td>
				<td>
					<table><tr><td>
					<div class="mainImgDataWrap">
						<div class="mainImageTarget">
						<?php  		
						$currentFile = $files[$selectedImgNum];
						$fv=$currentFile->getApprovedVersion();			
						echo $controller->getMainImgHTML($fv); 
						$detailsLinkTarget=$controller->getImageLink($fv,1);
						$fileCols=$controller->getDetailCols()
						?></div>
						
						<div class="imgInfo" <?php  if(!count($fileCols)){ ?>style="display:none;"<?php  } ?> >
                        
                        	<?php  if($detailsLinkTarget){ ?>
								<div class="imgOpts"><a href="<?php echo $detailsLinkTarget ?>"><?php echo  t('Details &raquo;') ?></a></div>
                            <?php  } ?>
							
							<div class="imgTitle" style="display:<?php echo in_array('title',$fileCols) ? 'block' : 'none' ?>"><?php echo htmlentities($fv->getTitle()) ?></div> 
                            
							<div class="proPhoto_imgDetails" style="text-align:left; font-size:10px; padding-top:4px;">
                            
								<?php echo  $controller->getDetailsHTML($fv) ?>	
							</div>
							<div class="spacer"></div> 
						</div>
					</div>
					</td></tr></table>
				</td>
				<td class="arrowRightCell">
					<?php 
					$nextImgNum = (($selectedImgNum+2)<=count($files)) ? $selectedImgNum+1 : $selectedImgNum=0; 
					$nextImgFile=$files[$nextImgNum];
					?>				
					<a href="<?php echo View::url($c->getCollectionPath().'?pp_fID_'.$bID.'='.intval($nextImgFile->getFileID()).'&img_num='.$nextImgNum)?>"><div class="arrowRight"><div class="arrowRightInner"></div></div></a>
				</td>
			</tr></table>			
			
		</div>
		<?php  } ?>
	
		
		<?php  if( $listMode!='grid' ){ ?>
		
		<div class="proPhotoFilmStrip proPhotoList" <?php echo ($listMode=='none')?'style="display:none"':''; ?>> 
			<div class="proPhotoFilmStripInner">
				<?php   
				$resultNum=0;
				
				//loop through all files
				foreach($files as $file){ 
					$resultNum++; 
					$fv = $file->getApprovedVersion(); 
					
					//$imageThumb = $ih->getThumbnail( $file, $maxThumbWidth, $maxThumbHeight);
					$thumbImgData = $controller->watermarkImage( $fv, $maxThumbWidth, $maxThumbHeight, 1 ); 
					
					$imageLinkTargetUrl=$controller->getImageLink( $fv, 0, $c ); 
					
					//large img path
					if(intval($controller->preload)){ 
						$largeImgData = $controller->watermarkImage( $fv, $controller->maxWidth, $controller->maxHeight ); 
						$preloadLrgUrl = $largeImgData->relPath; 
					}
					
					if($controller->downloadPrevention){
						$overlay = '<div class="proPhotoOverlay" style="height:'. $thumbImgData->height .'px; width:'.$thumbImgData->width.'px"></div>'; 
					}					
					?>
					<div id="proPhotoImgWrap<?php echo intval($bID)?>_<?php echo intval($file->getFileID()) ?>" class="proPhotoImgWrap <?php echo ( ($resultNum-1)==$selectedImgNum )?'selected':''?>">
					<?php  if($enlarge_mode!='none'){ ?><a class="proPhotoThumbLink" id="proPhotoThumbFID_<?php echo intval($file->getFileID())?>" <?php  if(strlen($imageLinkTargetUrl)){ ?>href="<?php echo $imageLinkTargetUrl ?>"<?php  } ?> 
						><?php  } ?><?php  if($listMode!='none'){ ?><?php echo $overlay ?><img src="<?php echo $thumbImgData->relPath ?>" alt="<?php echo str_replace('"','', $fv->getTitle()) ?>" class="proPhotoThumb"   
					/><?php  } ?><?php  if($enlarge_mode!='none'){ ?></a><?php  } ?> 
					
					<?php  if($preloadLrgUrl){ ?> 
						<var id="proPhotoPreload<?php echo $file->getFileID() ?>" class="proPhotoPreload"><?php echo $preloadLrgUrl ?></var>
					<?php  } ?>							
					
					</div> 			
				<?php  } ?> 
			</div>			
		</div>			
		
		<div class="proPhotoFilmStrip-slider" style="display:none"></div>  
		
		<?php  }else{ /* Thumbs Grid */ ?>		
		
		<table class="proPhotoList">
			<tr>
				<?php  
				$numOfCols=3;
				$resultNum=0;
				//loop through all files
				foreach($files as $file){  
					if($resultNum>0 && $resultNum%$numOfCols==0) echo '</tr><tr>';
					$resultNum++; 
					$fv = $file->getApprovedVersion(); 
					
					//$imageThumb = $ih->getThumbnail( $file, $maxThumbWidth, $maxThumbHeight);
					$thumbImgData = $controller->watermarkImage( $fv, $maxThumbWidth, $maxThumbHeight, 1 ); 
					
					$imageLinkTargetUrl=$controller->getImageLink( $fv ); 
					
					//large img path
					if(intval($controller->preload)){ 
						$largeImgData = $controller->watermarkImage( $fv, $controller->maxWidth, $controller->maxHeight ); 
						$preloadLrgUrl = $largeImgData->relPath; 
					}
					
					if($controller->downloadPrevention){
						$overlay = '<div class="proPhotoOverlay" style="height:'. $thumbImgData->height .'px; width:'.$thumbImgData->width.'px"></div>'; 
					}	
					
					?>
										
					<td id="pro-photo-list-item<?php echo intval($bID)?>_<?php echo $resultNum ?>" class="pro-photo-list-item" style="width:<?php echo round(99/$numOfCols) ?>%"> 
						<?php  if( $enlarge_mode!='link' ){ ?>
							<div id="proPhotoImgWrap<?php echo intval($bID)?>_<?php echo intval($file->getFileID()) ?>" class="proPhotoImgWrap <?php echo ( ($resultNum-1)==$selectedImgNum )?'selected':''?>">
							<?php  if($enlarge_mode!='none'){ ?><a class="proPhotoThumbLink" id="proPhotoThumbFID_<?php echo intval($file->getFileID())?>"  <?php  if(strlen($imageLinkTargetUrl)){ ?>href="<?php echo $imageLinkTargetUrl ?>"<?php  } ?> 
								><?php  } ?><img class="proPhotoThumb" src="<?php echo $thumbImgData->relPath ?>" alt="<?php echo str_replace('"','', $fv->getTitle()) ?>"  
							/><?php  if($enlarge_mode!='none'){ ?></a><?php  } ?> 
							</div> 
						<?php  }else{ ?> 
							<div id="proPhotoImgWrap<?php echo intval($bID)?>_<?php echo intval($file->getFileID()) ?>" class="proPhotoImgWrap">
							<?php  if($enlarge_mode!='none'){ ?><a class="proPhotoThumbLink" <?php  if(strlen($imageLinkTargetUrl)){ ?>href="<?php echo $imageLinkTargetUrl ?>"<?php  } ?> 
								><?php  } ?><img class="proPhotoThumb" src="<?php echo $thumbImgData->relPath ?>" alt="<?php echo str_replace('"','', $fv->getTitle()) ?>"   
							/><?php  if($enlarge_mode!='none'){ ?></a><?php  } ?>
							</div>
						<?php  } ?>
						
						<?php  if($preloadLrgUrl){ ?> 
							<var id="proPhotoPreload<?php echo $file->getFileID() ?>" class="proPhotoPreload"><?php echo $preloadLrgUrl ?></var>
						<?php  } ?>
					</td> 			
							
				<?php  } 
				 
				if(intval($resultNum%$numOfCols)) for($i=0;$i<($numOfCols-($resultNum%$numOfCols));$i++){
					echo '<td>&nbsp;</td>';
				} 
				?> 
			</tr>
		</table>
		
		<?php  } /* listMode */?>
		
		
	<?php  }  ?> 
	
	<?php 
	if($paging && $paginator && strlen($paginator->getPages())>0){ ?>	
	<div class="pagination">	
		 <span class="pageLeft"><?php  echo $paginator->getPrevious('Previous')?></span>
		 <?php  echo $paginator->getPages()?>
		 <span class="pageRight"><?php  echo $paginator->getNext('Next')?></span>
	</div>	
	<?php  } ?>	
</div>
 