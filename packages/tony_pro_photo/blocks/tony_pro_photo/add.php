<?php 
defined('C5_EXECUTE') or die(_("Access Denied.")); 
 
$controller->shownAttributes='title,description'; 
$controller->selectedImgColor='#0088FF';

//overriding the controller max widths, to work better with default plain yogurt theme
$controller->maxWidth=350; 
$controller->maxThumbWidth=250;
$controller->setMode='any'; 
$controller->watermarkAlpha=20;

$bt->inc('form_setup_html.php', array('controller' => $controller ));  
?>