var proPhotoFormHelper = { 

	init:function(){ 
	
		this.tabSetup(); 
		
		this.servicesURL=$('#proPhoto_servicesURL').val();
	
		$('input[name=enlarge_mode]').each(function(i,el){ 								
			$(el).click(function(){ proPhotoFormHelper.showTargetWrap(); proPhotoFormHelper.showLargeImgOpts(); } );
			$(el).change(function(){ proPhotoFormHelper.showTargetWrap(); proPhotoFormHelper.showLargeImgOpts(); } );
		});
		
		$('input[name=regionHeightOpt]').each(function(i,el){ 								
			$(el).click(function(){ proPhotoFormHelper.showRegionSize(); } );
			$(el).change(function(){ proPhotoFormHelper.showRegionSize(); } );
		});		
		
		$('input[name=listMode]').each(function(i,el){ 								
			$(el).click(function(){ proPhotoFormHelper.showMaxThumbSize(); } );
			$(el).change(function(){ proPhotoFormHelper.showMaxThumbSize(); } );
		});	
		
		$('input[name=watermarkType]').each(function(i,el){ 								
			$(el).click(function(){ proPhotoFormHelper.showWatermarkOptions(); } );
			$(el).change(function(){ proPhotoFormHelper.showWatermarkOptions(); } );
		});				
		
		$('#proPhoto_imageOrderPopup').click(function(){
			proPhotoFormHelper.showCustomOrderPopup();				  
		})
		
	},	 
	
	tabSetup: function(){
		$('ul#ccm-blockEditPane-tabs li a').each( function(num,el){ 
			el.onclick=function(){
				var pane=this.id.replace('ccm-blockEditPane-tab-','');
				proPhotoFormHelper.showPane(pane);
			}
		});		
	}, 
	
	showPane:function(pane){
		$('ul#ccm-blockEditPane-tabs li').each(function(num,el){ $(el).removeClass('ccm-nav-active') });
		$(document.getElementById('ccm-blockEditPane-tab-'+pane).parentNode).addClass('ccm-nav-active');
		$('div.ccm-blockEditPane').each(function(num,el){ el.style.display='none'; });
		$('#ccm-blockEditPane-'+pane).css('display','block'); 
	},	
	
	showLargeImgOpts:function(){  
		var d = ($('input[name=enlarge_mode]:checked').val()=='large_image') ? 'block':'none'; 
		$('#proPhoto_maxSizeWrap').css('display',d);
		$('#proPhoto_imgDetailsWrap').css('display',d);
	},	
	
	showMaxThumbSize:function(){  
		var d = ($('input[name=listMode]:checked').val()!='none') ? 'block':'none'; 
		$('#proPhoto_maxThumbSizeWrap').css('display',d);
	},
	
	showRegionSize:function(){  
		var d = ($('input[name=regionHeightOpt]:checked').val()=='fixed') ? 'inline':'none'; 
		$('#ccm-prophoto-regionHeight-wrap').css('display',d);
	},			
	
	showTargetWrap:function(){  
		var d = ($('input[name=enlarge_mode]:checked').val()=='none') ? 'none':'block'; 
		$('#ccm-proPhoto-imageTargetWrap').css('display',d);
	},
	
	showTagsTargetWrap:function(cb){  
		var d = (cb.checked) ? 'block':'none'; 
		$('#ccm-proPhoto-imageTagsTargetWrap').css('display',d);
	},	
	
	showWatermarkOptions:function(){ 
		
		var val = $('input[name=watermarkType]:checked').val(); 
		
		if( val=='image' ){ 
			posC='block';
			imgW='block';
			txtW='none';
			opts='block';
		}else if( val=='text' ) {
			posC='none';
			imgW='none';
			txtW='block';
			opts='block';
		}else{
			posC='none';
			imgW='none';
			txtW='none';
			opts='none';
		}	
		
		$('#proPhoto_watermarkTypeImgWrap').css('display',imgW);
		$('#proPhoto_watermarkTypeTextWrap').css('display',txtW);
		$('#proPhoto_watermarkPositionCenter').css('display',posC);
		$('#proPhoto_watermarkOptions').css('display',opts);
	},
	
	noImagesDisplayedError:'No image interface be displayed with these options',
	validate:function(){
			var failed=0; 
			
			if( $('input[name=listMode]:checked').val()=='none' && $('input[name=enlarge_mode]:checked').val()!='large_image' ){
				alert(this.noImagesDisplayedError); 
				failed=1;
			}
			
			if(failed){
				ccm_isBlockError=1;
				return false;
			}
			return true;
	},
	
	showCustomOrder:function(sel){
		if( $(sel).val()=='custom_sequence' ){
			$('#ccm-custom-display-sequence-wrap').css('display','block');
			this.showCustomOrderPopup();
		}else{
			$('#ccm-custom-display-sequence-wrap').css('display','none');
		}
	},
	
	showCustomOrderPopup:function(){
		
		var fIDs=$('#displayOrderIdSequence').val(),
			tags=$('#proPhoto_edit_tags').val(),
			setMode=$('input[name=setMode]:checked').val();
			setIds=[];
			
		$('input[name=fsIDs[]]:checked').each(function(){
			setIds.push(parseInt(this.value))
		});	
			
		var url = this.servicesURL+'?mode=sort&tags='+tags+'&setMode='+setMode+'&setIds='+setIds.join(',')+'&idSequence='+fIDs,
			title = $('#proPhoto_sortTitle').val();
		
		$.fn.dialog.open({
			href: url,
			title: title,
			width: 520,
			height: 300,
			modal: false, 
			onOpen: function () {
				$( "#proPhoto_sortableImages" ).sortable({
					stop:function(){ 
						proPhotoFormHelper.saveCustomOrder();
					}, 	
				});

			}
		});
	},
	
	saveCustomOrder:function(){
		var fIDs=[];
		$( "#proPhoto_sortableImages var.fID").each(function(el){
			if(!this.innerHTML || parseInt(this.innerHTML)<=0) return false;												 
			var fID=parseInt(this.innerHTML);												 
			fIDs.push(fID);								
		});
		$('#displayOrderIdSequence').val( fIDs.join(',') );
	},
	
	showDetailTarget:function(cb){ 
		var display=(cb.checked)?'block':'none';
		$('#ccm-prophoto-detail-page-target-wrap').css('display',display);
	},
	
	showTagsTarget:function(cb){ 
		var display=(cb.checked)?'block':'none';
		$('#ccm-prophoto-tags-page-target-wrap').css('display',display);
	},
	
	//the page selector js wasn't working with multiple instances 
	setPageTarget:function(cID, cName){  
		var p = $(ccmActivePageField.parentNode); 
		p.find('input').val(cID); 
		p.find('.ccm-summary-selected-item-label').html(cName);
	}
}

$(function(){proPhotoFormHelper.init();});
 
ccmValidateBlockForm = function() { return proPhotoFormHelper.validate(); }