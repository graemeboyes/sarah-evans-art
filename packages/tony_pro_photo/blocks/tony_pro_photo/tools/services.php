<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('file');
Loader::library('view');
$json = Loader::helper('json'); 

$jsonData=array();

switch ($_REQUEST['mode']){
	case 'largeImg': 
		$file=File::getById($_REQUEST['fID']);
		$tagsCID=intval($_REQUEST['tagsCID']);
		
		$b=Block::getById(intval($_REQUEST['bID']));
		if( !is_object($b) || $b->getBlockTypeHandle() != 'tony_pro_photo'){
			//this is used when hardcoding the block to a page 
			$proPhotoBlockController = new TonyProPhotoBlockController();
			$proPhotoBlockController->maxHeight=intval($_SESSION['pro_photo_max_height']); 
			$proPhotoBlockController->maxWidth=intval($_SESSION['pro_photo_max_width']);
			$proPhotoBlockController->shownAttributes=$_SESSION['pro_photo_attributes'];
			$proPhotoBlockController->downloadPrevention=intval($_SESSION['pro_photo_downloadPrevention']); 
		}else{  
			$proPhotoBlockController = $b->getInstance();
		}
		$maxHeight=$proPhotoBlockController->maxHeight; 
		$maxWidth=$proPhotoBlockController->maxWidth; 
		
		if(!$file || !$file->getFileId() ){
			$jsonData['error']=t('File Not Found');
		}else{
			$fv = $file->getApprovedVersion(); 
			$sizes=getimagesize( $fv->getPath() );   
			if($sizes[0]>$maxWidth){
				$sizes[1]=$maxWidth*($sizes[1]/$sizes[0]);
				$sizes[0]=$maxWidth;
			}
			if($sizes[1]>$maxHeight){ 			
				$sizes[0]=$maxHeight*($sizes[0]/$sizes[1]); 
				$sizes[1]=$maxHeight;
			}			
			
			if(intval($sizes[0])==0) $sizes[0]=600;
			if(intval($sizes[1])==0) $sizes[1]=400; 
			
			$jsonData['imgH'] = round($sizes[1]);
			$jsonData['imgW'] = round($sizes[0]);
			$jsonData['html']= $proPhotoBlockController->getMainImgHTML($fv);  
			$jsonData['title']= $fv->getTitle();
			
			$tags = $fv->getTagsList();
			$tagLinks='';
			foreach($tags as $tag){ 
				if($notFirst) $tagLinks.=', ';
				$tagLinks.='<a href="'.View::url('/photo_search?tag='.$tag).'">'.$tag.'</a>';
				$notFirst=1;
			} 		
			$jsonData['tagLinks']=$tagLinks; 
			
			$jsonData['details_html'] = $proPhotoBlockController->getDetailsHTML($fv,$tagsCID); 
		} 
		break;
	
	
	case 'sort':
	
		$proPhotoBlockController = new TonyProPhotoBlockController();
		
		$cleanFIDs=array();
		$fIDs=($_REQUEST['idSequence'])?explode(',',$_REQUEST['idSequence']):array();
		foreach($fIDs as $fID) 
			if(intval($fID)>0) $cleanFIDs[]=intval($fID);
		
		$cleanSetIDs=array();
		$setIDs=($_REQUEST['setIds'])?explode(',',$_REQUEST['setIds']):array();
		foreach($setIDs as $setID) 
			if(intval($setID)>0) $cleanSetIDs[]=intval($setID);	
		
		
		$opts = array('displayOrder'=>'custom_sequence', 
					  'displayOrderIdSequence'=>join(',',$cleanFIDs),
					  'tags'=>$_REQUEST['tags'], 
					  'setMode'=>$_REQUEST['setMode'],
					  'setIds'=>join(',',$cleanSetIDs)
				);
		
		$fileList = $proPhotoBlockController->getFileList($opts);
		$files = $fileList->get( 100, 0);	
		
		echo '<div id="proPhoto_sortableImagesTitle">'.t("Drag images to change their order").'</div>';
		echo '<ul id="proPhoto_sortableImages">';
		$ih = Loader::helper('image'); 
		if(!count($files)) echo t('No results found');
		else foreach($files as $file){ 
			//$fv = $file->getApprovedVersion(); 
			$imageThumb = $ih->getThumbnail( $file, 200, 50); 
			echo '<li><var class="fID">'.$file->getFileID().'</var><img src="'.$imageThumb->src.'" /></li>';
		} 
		echo '</ul>';
		
		die; 
		break; 
		
	default: 
		$jsonData['error']=t('Error: no service mode specified.'); 
		break;
}

echo $json->encode( $jsonData );

?>