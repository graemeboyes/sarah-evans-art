<?php  
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$textHelper = Loader::helper("text");
	$imgHelper = Loader::Helper('image');
	// now that we're in the specialized content file for this block type, 
	// we'll include this block type's class, and pass the block to it, and get
	// the content

if (count($cArray) > 0) { ?>
<!-- projects --> 
<ul class="projects">
<?php  
	for ($i = 0; $i < count($cArray); $i++ ) {
		$cobj = $cArray[$i]; 
		$title = $cobj->getCollectionName();
		$link = $nh->getLinkToCollection($cobj);
		?>	

<li class="hasprj" data-id="id-<?php  echo $i?>" data-type="<?php  echo $pt = $cobj->getCollectionAttributeValue('page_category')?>">
<div class="boxgrid captionfull">
<?php  

								$pt = $cobj->getCollectionAttributeValue('page_thumbnail');
								if($pt){
									$imgHelper->outputThumbnail($pt, 225,120, $title);
								}
								?>
<div class="cover boxcaption">
<h3><?php 
	print("<a href=\"$link\" class=\"picbg view\">");
	print $title;
	print("</a>");
	?></h3>
</div>
</div>
</li>
<?php    } ?>	
							</ul>
<?php  
	if(!$previewMode && $controller->rss) { 
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$uh = Loader::helper('concrete/urls');
			$rssUrl = $controller->getRssUrl($b);
			?>
			<div class="rssIcon">
				<a href="<?php   echo $rssUrl?>" target="_blank"><img src="<?php  echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /></a> <a href="<?php   echo $rssUrl?>" target="_blank"><?php  echo $controller->rssTitle?></a>
			</div>
		<?php   
	} 
	?>

<?php   } 
	
	if ($paginate && $num > 0 && is_object($pl)) {
		$pl->displayPaging();
	}
	
?>