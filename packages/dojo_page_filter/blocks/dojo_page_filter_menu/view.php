<?php   
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$textHelper = Loader::helper("text");
?>
<ul class="dif-menu">
<?php    
	$options = $controller->getdifCategories();
	foreach($options as $opt) { ?>
		<li class="dif-list" id="<?php   echo $opt->getOptionName() ?>"><a href="<?php   echo $opt->getOptionName() ?>" onclick="javascript:qcksand('<?php   echo $opt->getOptionName() ?>');return false;"><?php   echo $opt->getOptionName() ?></a></li>
	<?php    } ?>
</ul>
