<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
class DojoPageFilterMenuBlockController extends BlockController {
	 
	protected $btTable = 'btDojoPageFilterMenu';
	protected $btInterfaceWidth = "420";
	protected $btInterfaceHeight = "300";	
	protected $btIncludeAll = 1;
	
	var $options = array();

	/** 
	 * Used for localization. If we want to localize the name/description we have to include this
	 */
	public function getBlockTypeDescription() {
		return t("Menu for the Dojo Page Filter.");
	}
	
	public function getBlockTypeName() {
		return t("Dojo Page Filter Menu");
	} 
	
	function __construct($obj = NULL) {
		parent::__construct($obj);
		$c = Page::getCurrentPage();
		if (is_object($c)) {
			$this->cID = $c->getCollectionID();
		}
		if($this->bID) {
			$db = Loader::db();
			$v = array($this->bID);
			$q = "select optionID, optionName, displayOrder from btSurveyOptions where bID = ? order by displayOrder asc";
			$r = $db->query($q, $v);
			$this->options = array();
			if ($r) {
				while ($row = $r->fetchRow()) {
					$opt = new BlockDojoPageFilterMenu;
					$opt->optionID = $row['optionID'];
					$opt->optionName = $row['optionName'];
					$opt->displayOrder = $row['displayOrder'];
					$this->options[] = $opt;
				}
			}
		}
	}
	
	function getdifCategories() { return $this->options; }
	


	function duplicate($newBID) {
		
		$db = Loader::db();
		
		foreach($this->options as $opt) {
			$v1 = array($newBID, $opt->getOptionName(), $opt->getOptionDisplayOrder());
			$q1 = "insert into btSurveyOptions (bID, optionName, displayOrder) values (?, ?, ?)";
			$db->query($q1, $v1);
			
			$v2 = array($opt->getOptionID());
			$newOptionID = get_insert_id();
			$q2 = "select * from btSurveyResults where optionID = ?";
			$r2 = $db->query($q2, $v2);
			if ($r2) {
				while ($row = $r2->fetchRow()) {
					$v3 = array($newOptionID, $row['uID'], $row['ipAddress'], $row['timestamp']);
					$q3 = "insert into btSurveyResults (optionID, uID, ipAddress, timestamp) values (?, ?, ?, ?)";
					$db->query($q3, $v3);
				}
			}
		}
		
		return parent::duplicate($newBID);
		
	}
	
	function save($args) {
		parent::save($args);
		$db = Loader::db();
		
		if(!is_array($args['survivingOptionNames'])) 
			$args['survivingOptionNames'] = array();
 
		$slashedArgs=array();
		foreach($args['survivingOptionNames'] as $arg)
			$slashedArgs[]=addslashes($arg); 
		$db->query("DELETE FROM btSurveyOptions WHERE optionName NOT IN ('".implode("','",$slashedArgs)."') AND bID = ".intval($this->bID) );
			
		if (is_array($args['difCategory'])) {
			$displayOrder = 0;
			foreach($args['difCategory'] as $optionName) {
				$v1 = array($this->bID, $optionName, $displayOrder);	
				$q1 = "insert into btSurveyOptions (bID, optionName, displayOrder) values (?, ?, ?)";
				$db->query($q1, $v1);
				$displayOrder++;
			}
		}
	
	}	
	
	
}

class BlockDojoPageFilterMenu {
	var $optionID, $optionName, $displayOrder;
	
	function getOptionID() {return $this->optionID;}
	function getOptionName() {return $this->optionName;}
	function getOptionDisplayOrder() {return $this->displayOrder;}
	
	function getResults() {
		$c = Page::getCurrentPage();
		$db = Loader::db();
		$v = array($this->optionID, intval($c->getCollectionID()) );
		$q = "select count(resultID) from btSurveyResults where optionID = ? AND cID=?";
		$result = $db->getOne($q, $v);
		if ($result > 0) {
			return $result;
		} else {
			return 0;
		}
	}
}


?>