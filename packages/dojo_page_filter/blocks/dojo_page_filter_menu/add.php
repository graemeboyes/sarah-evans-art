<?php    defined('C5_EXECUTE') or die(_("Access Denied.")); ?> 
<style type="text/css">
div.dif-category-menu {
	position: relative; border-bottom: 1px solid #ddd; padding-bottom: 3px; padding-top: 3px;
}

div.dif-category-menu img {
	position: absolute; top: 3px; right: 0px;
}
</style>
<script>
var currentOption = 0;
</script>

<strong><?php   echo t('Options')?></strong>
<div id="difCategories">
<?php   echo t('None')?>
</div>

<br/><br/>
<strong><?php   echo t('Add option')?></strong><br/>
<input type="text" name="optionValue" id="dif-optionValue" style="width: 350px" />
<input type="button" onclick="addOption()" value="<?php   echo t('Add')?>" />