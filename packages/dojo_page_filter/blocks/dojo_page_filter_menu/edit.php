<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
?>

<script type="text/javascript">
	var currentOption = <?php   echo count($controller->options)?>;
</script>
<style type="text/css">
div.dif-category-menu {
	position: relative; border-bottom: 1px solid #ddd; padding-bottom: 3px; padding-top: 3px;
}

div.dif-category-menu img {
	position: absolute; top: 3px; right: 0px;
}
</style>

<strong><?php   echo t('Options')?></strong>
<div id="difCategories">
<?php    
$options = $controller->getdifCategories();
if (count($options) == 0) {
	echo t("None");
} else {
	foreach($options as $opt) { ?>		
        <div class="dif-category-menu" id="option<?php   echo $opt->getOptionID()?>"><a href="#" onclick="removeOption(<?php   echo $opt->getOptionID()?>)"><img src="<?php   echo ASSETS_URL_IMAGES?>/icons/delete_small.png" /></a> <?php   echo $opt->getOptionName()?>
        <input type="hidden" name="survivingOptionNames[]" value="<?php   echo htmlspecialchars($opt->getOptionName())?>" />
        </div>		
	<?php    }
} ?>
</div>
<br/><br/>
<strong><?php   echo t('Add option')?></strong><br/>
<input type="text" name="optionValue" id="dif-optionValue" style="width: 350px" />
<input type="button" onclick="addOption()" value="<?php   echo t('Add')?>" />