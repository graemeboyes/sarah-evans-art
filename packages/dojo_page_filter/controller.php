<?php       

defined('C5_EXECUTE') or die(_("Access Denied."));

class DojoPageFilterPackage extends Package {

	protected $pkgHandle = 'dojo_page_filter';
	protected $appVersionRequired = '5.3.0';
	protected $pkgVersion = '1.3'; 
	
	public function getPackageName() {
		return t("Dojo Page Filter"); 
	}	
	
	public function on_page_view() {
				$html = Loader::helper('html');			
				$this->addHeaderItem($html->javascript($bv->getBlockURL() . '/java/jquery-ui-1.7.3.custom.min'));
$this->addHeaderItem($html->javascript($bv->getBlockURL() . '/java/jquery.easing.1.3.js'));
$this->addHeaderItem($html->javascript($bv->getBlockURL() . '/java/jquery.quicksand.js'));
$this->addHeaderItem($html->javascript($bv->getBlockURL() . '/java/quicksand.sorting.js'));
			}
	
	public function getPackageDescription() {
		return t("Reorder and filter pages with a nice shuffling animation.");
	}
	
public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('dojo_page_filter_menu', $pkg);		
		BlockType::installBlockTypeFromPackage('page_list', $pkg);		
	}

function getFileID() {return $this->fID;}
		function getFileObject() {
			return File::getByID($this->fID);

	}
}

?>