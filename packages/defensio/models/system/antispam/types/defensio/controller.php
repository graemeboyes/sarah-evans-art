<?php  

class DefensioSystemAntispamTypeController extends Object {
	
	public function saveOptions($args) {
		$pkg = Package::getByHandle('defensio');
		$pkg->saveConfig('ANTISPAM_DEFENSIO_API_KEY', $args['ANTISPAM_DEFENSIO_API_KEY']);
	}

	public function check($args) {
		$pkg = Package::getByHandle('defensio');
		Loader::library('3rdparty/defensio/defensio', 'defensio');
		$c = Page::getCurrentPage();
		if (is_object($c)) { 
			$link = Loader::helper('navigation')->getLinkToCollection($c, true);
		} else { 
			$link = BASE_URL . DIR_REL;
		}
		$v = array();
		$v['user_ip'] = $args['ip_address'];
		$v['user_agent'] = $args['user_agent'];
		$comment=array('type' => 'comment', 'content' => $args['content'], 'platform' => 'concrete5', 'author-ip'=> $args['ip_address'], 'client' => 'Concrete5 Defensio | 1.0 | Jack Lightbody | jack.lightbody@gmail.com');
		$u=new User();
		if($u->isLoggedIn()){
			$comment['author-logged-in']=true;
		}
		$defensio = new Defensio($pkg->config('ANTISPAM_DEFENSIO_API_KEY'));
		$result = $defensio->postDocument($comment);
		if($result[1][allow]==true){
			return true;
		}else{
			return false;
		}
		
		
		
	}

}