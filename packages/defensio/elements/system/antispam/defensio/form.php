<?php   

defined('C5_EXECUTE') or die(_("Access Denied."));

$form = Loader::helper('form');
$pkg = Package::getByHandle('defensio');
?>

<div class="clearfix">
	<?php   echo $form->label('ANTISPAM_DEFENSIO_API_KEY', t('API Key'))?>
	<div class="input">
		<?php   echo $form->text('ANTISPAM_DEFENSIO_API_KEY', $pkg->config('ANTISPAM_DEFENSIO_API_KEY'), array('class' => 'span4'))?>
	</div>
</div>