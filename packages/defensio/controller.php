<?php   

defined('C5_EXECUTE') or die(_("Access Denied."));

class DefensioPackage extends Package {

	protected $pkgHandle = 'defensio';
	protected $appVersionRequired = '5.5';
	protected $pkgVersion = '1.0';
	
	public function getPackageDescription() {
		return t("Adds defensio as an anti-spam service.");
	}
	
	public function getPackageName() {
		return t("Defensio");
	}
	
	public function install() {
		$pkg = parent::install();
		Loader::model('system/antispam/library');
		SystemAntispamLibrary::add('defensio', t('Defensio'), $pkg);		
	}
	
}