<?php      
defined('C5_EXECUTE') or die(_("Access Denied."));
class MylabGalleriaPackage extends Package {

	protected $pkgHandle = 'mylab_galleria';
	protected $appVersionRequired = '5.3.3';
	protected $pkgVersion = '1.0.1';
	
	public function getPackageDescription() {
		return t("Provide a full options Jquery Galleria block");
	}
	
	public function getPackageName() {
		return t("Jquery Galleria");
	} 
	
	public function install() {
		$pkg = parent::install();
		BlockType::installBlockTypeFromPackage('mylab_galleria', $pkg);
	}
}