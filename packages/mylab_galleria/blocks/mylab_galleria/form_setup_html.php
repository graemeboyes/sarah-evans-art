<?php      
defined('C5_EXECUTE') or die(_("Access Denied."));

$al = Loader::helper('concrete/asset_library');
$ah = Loader::helper('concrete/interface');
$colorh = Loader::helper('form/color');
$af = Loader::helper('form/attribute');

Loader::model('file_set');
Loader::model('file_attributes');

$fileAttributes = FileAttributeKey::getList(); 
$s1 = FileSet::getMySets();
$fsa = array("" => "Select a file set");
if (!$fsID) {
        $width          =       $controller->width_default;
        $height         =       $controller->height_default;
        $carousel       =       $controller->carousel_default;
        $carouselFollow =       $controller->carouselFollow_default;
        $carouselSpeed  =       $controller->carouselSpeed_default;
        $carouselSteps  =       $controller->carouselSteps_default;
        $imageCrop      =       $controller->imageCrop_default;
        $thumbCrop      =       $controller->thumbCrop_default;
        $imageMargin    =       $controller->imageMargin_default;
        $thumbMargin    =       $controller->thumbMargin_default;
        $maxScaleRatio  =       $controller->maxScaleRatio_default;
        $popupLinks     =       $controller->popupLinks_default;
        $preload        =       $controller->preload_default;     
        $thumbnails     =       $controller->thumbnails_default;
        $transition     =       $controller->transition_default;
        $transitionSpeed=       $controller->transitionSpeed_default;
        $history        =       $controller->history_default;
        $slideShow      =       $controller->slideShow_default;
        $slideShowSpeed =       $controller->slideShowSpeed_default;
        $myColor        =       $controller->myColor_default;
}

?>

<style type="text/css">
.ccm-gallery-options-tab * {
	margin:0;
	padding:0;
}
.ccm-gallery-options-tab ul {
	margin:10px;
	list-style:none;
}
.ccm-gallery-options-tab ul li {
	margin-bottom:15px;
}
label {
	padding-top:.5em; margin-top:1em;
	border-top:1px solid #cccccc;
}
input[type="text"], select  {
        float:right;
        margin-top:-1.6em;
        
}
</style>
<ul class="ccm-dialog-tabs" id="ccm-gallery-tabs">
  <li class="ccm-nav-active"><a href="javascript:void(0)" id="ccm-gallery-type"><?php      echo t('Gallery')?></a></li>
  <li><a href="javascript:void(0)" id="ccm-gallery-options"><?php      echo t('Advanced Options')?></a></li>
</ul>
<div id="ccm-gallery-options-tab" style="display:none">
        <table cellpadding="10" width="100%">
                <tr valign="top">
                        <td style="width:50%; padding:1.5em;">

        <label for="width"><?php      echo t('Width (0 for auto)');?></label>
        <input type="text" id="width" name="width" size="4" value="<?php    echo $width?>" />

        <label for="height"><?php      echo t('Height (0 for 16:9 ratio)');?></label>
        <input type="text" id="height" name="height" size="4" value="<?php    echo $height?>" />
 
                        </td>
                        <td style="width:50%; padding:1.5em;">

        <label for="imageMargin"><?php      echo t('Image margin');?></label>
        <input type="text" id="imageMargin" name="imageMargin" size="4" value="<?php    echo $imageMargin?>"/>
                                
        <label for="thumbMargin"><?php      echo t('Thumbnail margin');?></label>
        <input type="text" id="thumbMargin" name="thumbMargin" size="4" value="<?php    echo $thumbMargin?>"/>

        <label for="popupLinks"><?php      echo t('Open image in a new window');?></label>
        <input type="radio" name="popupLinks" value="1"         <?php    echo $popupLinks ==  1 ?'checked':''?>      />Yes, please
        <input type="radio" name="popupLinks" value="0"         <?php    echo $popupLinks ==  0 ?'checked':''?>      />No, Thank you!

        <label for="history"><?php      echo t('History for bookmarking, back-button etc');?></label>
        <input type="radio" name="history" value="1"         <?php    echo $history ==  1 ?'checked':''?>      />Yes, please
        <input type="radio" name="history" value="0"         <?php    echo $history ==  0 ?'checked':''?>      />No, Thank you!

        <label for="preload"><?php      echo t('Preload (number or "all")');?></label>
        <input type="text" id="preload" name="preload" size="4" value="<?php    echo $preload?>"/>
	
        <label for="transitionSpeed"><?php      echo t('Transition speed');?></label>
        <input type="text" id="transitionSpeed" name="transitionSpeed" size="4" value="<?php    echo $transitionSpeed?>"/>

        <label for="maxScaleRatio"><?php      echo t('maximum scale ratio (1=100%)');?></label>
        <input type="text" id="maxScaleRatio" name="maxScaleRatio" size="4" value="<?php    echo $maxScaleRatio?>"/>

        <label for="carouselFollow"><?php      echo t(' Follow carousel');?></label>
        <input type="radio" name="carouselFollow" value="1"    <?php    echo $carouselFollow == 1 ?'checked':''?>       />follow the active image
        <input type="radio" name="carouselFollow" value="0"    <?php    echo $carouselFollow == 0 ?'checked':''?>       />Don't follow
	
        <label for="carouselSpeed"><?php      echo t('Carousel speed (milliseconds)');?></label>
        <input type="text" id="carouselSpeed" name="carouselSpeed" size="4" value="<?php    echo $carouselSpeed?>"/>

        <label for="carouselSteps"><?php      echo t('Carousel steps (n or "auto")');?></label>
        <input type="text" id="carouselSteps" name="carouselSteps" size="4" value="<?php    echo $carouselSteps?>"/>

        <label for="imageCrop"><?php      echo t('Crop the picture');?></label>
        <input type="radio" name="imageCrop" value="1"          <?php    echo $imageCrop ==  1 ?'checked':''?>       />Yes, please
        <input type="radio" name="imageCrop" value="0"          <?php    echo $imageCrop ==  0 ?'checked':''?>       />No, Thank you!

        <label for="thumbCrop"><?php      echo t('Crop the Thumbnails');?></label>
        <input type="radio" name="thumbCrop" value="1"          <?php    echo $thumbCrop ==  1 ?'checked':''?>      />Yes, please
        <input type="radio" name="thumbCrop" value="0"          <?php    echo $thumbCrop ==  0 ?'checked':''?>      />No, Thank you!

                        </td>
                </tr>
        </table>

</div>
<div id="ccm-gallery-type-tab">

        <h2><span><?php    echo $fsID ? "Edit your Galleria":"New galleria with default settings"?></span></h2>
	<div class="backgroundRow">
                <table width="100%" cellpadding="10" style="margin-top:.2em;">
		<tr>
		<td style="width:50%; padding:1.5em">
                        <label for="fsID"><?php      echo t('File Set') ?>:</label>
                        <select name="fsID" title="fsID">
                                <?php      foreach($s1 as $fs) {
                                echo '<option value="'.$fs->getFileSetID().'" ' . ($fsID == $fs->getFileSetID() ? 'selected="selected"' : '') . '>'.$fs->getFileSetName().'</option>\n';
                                  }?>
                        </select>
                </td>
                <td style="width:50%; padding:1.5em">
                        <label for="fsTitle"><?php      echo t('Display as Image title') ?>:</label>
                        <select name="fsTitle">
                            <option value="title"       <?php     echo ($fsTitle == "title"?'selected':'')?>            >Title</option>
                            <option value="description" <?php     echo ($fsTitle == "description"?'selected':'')?>      >Description</option>
                            <option value="date"        <?php     echo ($fsTitle == "date"?'selected':'')?>             >Date Posted</option>
                            <option value="filename"    <?php     echo ($fsTitle == "filename"?'selected':'')?>         >File Name</option>
                            <?php     
                            foreach($fileAttributes as $ak) {  ?>
                                <option value="<?php     echo $ak->getAttributeKeyHandle() ?>"
                                                        <?php     echo ($fsTitle == $ak->getAttributeKeyHandle()?'selected':'')?> >         <?php     echo  $ak->getAttributeKeyName() ?>
                                </option>
                            <?php      } ?> 
                        </select>
                </td>
 		</tr>
		<tr>
                <td>&nbsp;</td>
                <td style="width:50%; padding:1.5em;">
                        <label for="fsDescription"><?php      echo t('Display as Image Description')?>:</label>
                        <select name="fsDescription" class="ccm-file-set-description">
                            <option value="title"       <?php     echo ($fsDescription == "title"?'selected':'')?>        >Title</option>
                            <option value="description" <?php     echo ($fsDescription == "description"?'selected':'')?>  >Description</option>
                            <option value="date"        <?php     echo ($fsDescription == "date"?'selected':'')?>         >Date Posted</option>
                            <option value="filename"    <?php     echo ($fsDescription == "filename"?'selected':'')?>     >File Name</option>
                            <?php     
                            foreach($fileAttributes as $ak) {  ?>
                                <option value="<?php     echo $ak->getAttributeKeyHandle()?>"
                                    <?php     echo ($fsDescription == $ak->getAttributeKeyHandle()?'selected':'')?>         ><?php     echo  $ak->getAttributeKeyName() ?>
                                </option>
                            <?php      } ?> 
                        </select>
                </td>
		</tr>
		</table>
        <h2><span>Basics options</span></h2>
                <table cellpadding="10" cellspacing="10" width="100%" bgcolor="#eee">
                <tr valign="top">
                        <td style="width:50%; padding:1.5em">

        <?php     echo $colorh->output('myColor', 'Background Color', $myColor) ?>

        <label for="slideShow"><?php      echo t('Slideshow ?');?></label>
        <input type="radio" name="slideShow" value="1"         <?php    echo $slideShow == 1 ?'checked':''?>      />Yes, please
        <input type="radio" name="slideShow" value="0"         <?php    echo $slideShow == 0 ?'checked':''?>      />No, Thank you!

        <label for="slideShowSpeed"><?php      echo t('Slideshow Speed (milliseconds)');?></label>
        <input type="text" id="slideShowSpeed" name="slideShowSpeed" size="4" value="<?php    echo $slideShowSpeed?>"/>


                        </td>
                        <td style="width:50%; padding:1.5em">
        <label for="thumbnails"><?php      echo t('Display Thumbnails');?></label>
        <input type="radio" name="thumbnails" value="1"         <?php    echo $thumbnails == 1 ?'checked':''?>      />Yes, please
        <input type="radio" name="thumbnails" value="0"         <?php    echo $thumbnails == 0 ?'checked':''?>      />No, Thank you!

         <label for="transition"><?php      echo t('Transition animation');?></label>
        <select name="transition" class="ccm-input-select ccm-file-set-id">
                <option value="fade"    <?php     echo ($transition == "fade"?'selected':'')?>      >fade</option>
                <option value="flash"   <?php     echo ($transition == "flash"?'selected':'')?>     >flash</option>
                <option value="slide"   <?php     echo ($transition == "slide"?'selected':'')?>     >slide</option>
                <option value="fadeslide"<?php     echo ($transition == "fadeslide"?'selected':'')?>>fadeslide</option>
        </select>
                               
                        </td>
                </tr>
        </table>

	</div>

        
</div>
<!-- end tab container -->
<!-- Tab Setup -->
<script type="text/javascript">
	var ccm_fpActiveTab = "ccm-gallery-type";	
	$("#ccm-gallery-tabs a").click(function() {
		$("li.ccm-nav-active").removeClass('ccm-nav-active');
		$("#" + ccm_fpActiveTab + "-tab").hide();
		ccm_fpActiveTab = $(this).attr('id');
		$(this).parent().addClass("ccm-nav-active");
		$("#" + ccm_fpActiveTab + "-tab").show();
	});
	
	$(document).ready(function() { 
		//FlashGalleryBlock.init(); 
	});
</script>
