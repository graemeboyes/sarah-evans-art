Galleria.themes.create({
    name: 'classic',
    author: 'Galleria',
    version: '1.0',
    css: 'galleria.classic.css',
    defaults: {
        transition: 'slide'
    },
    init: function(options) {
        
        var mc = Galleria.MAC && Galleria.CHROME;
        
        if (!mc) {
            this.$('thumbnails').children().hover(function() {
                $(this).not('.active').fadeTo(200, .4);
            }, function() {
                $(this).fadeTo(400, 1);
            });
        }
        // Container stuffs
        this.$('stage').css('background-color', this.options.my_color);
        this.$('container').height(this.stageHeight = ( this.options.height || Math.round(this.stageWidth*9/16) ) );
        if ( this.options.width > 0) {
            this.$('container').width(this.stageWidth = this.options.width);            
        }
        
        // Hide Thumbnails if needed
        if (!options.thumbnails) {
            this.$('thumbnails-container').css('display','none');
            this.$('stage').css('bottom','0');
        }

        // Normaly working but not here..
        //this.setCSS (cssText);

        
        this.rescale();
        this.$('loader').show().fadeTo(200, 0.4);
        this.$('counter').show().fadeTo(200, 0.4);
        
     // creates a play link and appends it to the container
        if (this.options.slide_show) {
            if (this.options.slide_show_speed < 50) { this.options.slide_show_speed = 100 };
            this.play( this.options.slide_show_speed);
            // Add SlideShow controls
            $('<a>').text('play').addClass('galleria-play').bind('click', this.proxy(function() {
                this.play( this.options.slide_show_speed);
                this.addElement('galleria-pause');
            })).appendTo(this.get('stage'));
            $('<a>').text('pause').addClass('galleria-pause').bind('click', this.proxy(function() {
                this.pause();
            })).appendTo(this.get('stage'));
        }
        
        
        //Galleria.log(this.$('galleria-pause'));

        this.$('container').hover(this.proxy(function() {
            this.$('image-nav-left,image-nav-right,counter').fadeIn(200);
            $('.galleria-play, .galleria-pause').fadeIn(200);
        }), this.proxy(function() {
            this.$('image-nav-left,image-nav-right,counter').fadeOut(500);
            $('.galleria-play, .galleria-pause').fadeOut(200);
        }));
        
        this.$('image-nav-left,image-nav-right,counter').hide();
        
        var elms = this.$('info-link,info-close,info-text').click(function() {
            elms.toggle();
        });
        
        this.attachKeyboard({
            left: this.prev, // applies the native prev() function
            right: this.next,
            up: function() {
                // custom up action
            },
            13: function() {
            // when return (keyCode 13) is pressed:
            }
        });

        
        this.bind(Galleria.LOADSTART, function(e) {
            if (!e.cached) {
                this.$('loader').show().fadeTo(200, .4);
            }
            if (this.hasInfo()) {
                this.$('info').show();
            } else {
                this.$('info').hide();
            }
            if (!mc) {
                $(e.thumbTarget).parent().css('opacity',1).addClass('active').siblings().removeClass('active');
            }
        });

        this.bind(Galleria.LOADFINISH, function(e) {
            this.$('loader').fadeOut(200);
        });
        
   
        
        this.show(0);
    }
});