<?php      
defined('C5_EXECUTE') or die(_("Access Denied."));

class MylabGalleriaBlockController extends BlockController {
	
	var $pobj;
	
	protected $btTable = 'btMylabGalleria';
	protected $btInterfaceWidth = "700";
	protected $btInterfaceHeight = "450";
	
	public $width_default	= 400;
	public $height_default	= 300;
	
	
	// Specific for the Galleria Jquery Gallery
	public $carousel_default	= true;
	public $carouselFollow_default	= true;
	public $carouselSpeed_default	= 200;
	public $carouselSteps_default	= 'auto';
	public $imageCrop_default	= false;
	public $thumbCrop_default	= true;
	public $imageMargin_default	= 0;
	public $thumbMargin_default	= 0;
	public $maxScaleRatio_default	= 1;
	public $popupLinks_default	= false;
	public $preload_default		= 2;
	public $thumbnails_default	= true;
	public $transition_default	= 'fade'; // flash , slide , fadeslide	
	public $transitionSpeed_default= 400;
	public $history_default		= true;
	public $slideShow_default	= false;
	public $slideShowSpeed_default	= 3000;
	public $myColor_default		= '#ffffff';

	public function __construct($obj = null) {		
		parent::__construct($obj);
		$this->db = Loader::db(); 
	}
		
	public function on_page_view() {
		
	}
	
	public function getBlockTypeDescription() {
		return t("Display a full options Galleria");
	}
	
	public function getBlockTypeName() {
		return t("Jquery Galleria");
	}
		
	public function getSortedImages() {
		if($this->bID) {
			// Check compatibility with Concrete 5.4.1
			if ($this->db->getAll("SHOW COLUMNS FROM FileSetFiles LIKE 'fsDisplayOrder'") ) {
				$data = $this->db->getAll("SELECT fsID,fID FROM FileSetFiles WHERE fsID=? ORDER BY fsDisplayOrder", array($this->fsID) );				
			} else {
				$data = $this->db->getAll("SELECT fsID,fID FROM FileSetFiles WHERE fsID=? ORDER BY timestamp", array($this->fsID) );				
			}

			Loader::model('file');
			$i = Loader::helper('image');
			
			$v = array();
			$cc = 0;
			if (is_array($data)) {
				foreach ($data as $d){
				
				  $img = File::getByID($d['fID']);
				  // this test take some time if you want to improve speed, delete from here
				  $fv = $img->getExtension();
				  Loader::library('file/types');
				  $ft = FileTypeList::getType($fv);
				  if ($ft->type == 1) {
				  // to here
					$s = @getimagesize($img->getPath());
					$v[$cc]['desc'] = 	$this->getFileFieldValue($img,$this->fsDescription);
					$v[$cc]['title'] =	$this->getFileFieldValue($img,$this->fsTitle);
					$v[$cc]['width'] = 	$s[0] ;
					$v[$cc]['height'] = 	$s[1] ;
					$v[$cc]['src'] = 	$img->getRelativePath();
					$cc ++;		
					
				  }
				}
				
			} else {
				return false;
			}
			return $v;
		} else {
			return false;
		}
	}
	private function is_bo ($b) {
		if ($b == '0' || $b == '1' ) return true;
		else return false;
	}
	
	private function getFileFieldValue($f,$handle) {
		if(!is_object($f)) {
			return false;
		}
		$value = "";
		switch($handle) {
			case "title";
				$value = $f->getTitle();
			break;
			case "description";
				$value = $f->getDescription();
			break;
			case "date":
				$value = $f->getDateAdded();
			break;
			case "filename":
				$value = $f->getFileName();
			break;
			default:
				$value = $f->getAttribute($handle);
			break;
		}
		return $value;
}

	public function save($data) {
		$data['width'] 		= (is_numeric($data['width']) 		&& $data['width'] >= 0) 		? $data['width'] :  $this->width_default; 
		$data['height'] 	= (is_numeric($data['height']) 		&& $data['height'] >= 0) 	? $data['height'] :  $this->height_default; 
		$data['transitionSpeed']= (is_numeric($data['transitionSpeed']) && $data['transitionSpeed'] >= 0) ? $data['transitionSpeed'] :  $this->transitionSpeed_default; 
		$data['maxScaleRatio'] 	= (is_numeric($data['maxScaleRatio']) 	&& $data['maxScaleRatio'] >= 0 && $data['maxScaleRatio'] <= 1) ? $data['maxScaleRatio'] :  $this->maxScaleRatio_default; 
		$data['carouselSpeed'] = (is_numeric($data['carouselSpeed']) 	&& $data['carouselSpeed'] >= 0) ? $data['carouselSpeed'] :  $this->carouselSpeed_default; 
		$data['carouselSteps'] = (is_numeric($data['carouselSteps']) 	&& $data['carouselSteps'] >= 1) ? $data['carouselSteps'] :  $this->carouselSteps_default; 
		$data['imageMargin'] = (is_numeric($data['imageMargin']) 	&& $data['imageMargin'] >= 0) ? $data['imageMargin'] :  $this->imageMargin_default; 
		$data['thumbMargin'] = (is_numeric($data['thumbMargin']) 	&& $data['thumbMargin'] >= 0) ? $data['thumbMargin'] :  $this->thumbMargin_default; 
		$data['slideShowSpeed'] = (is_numeric($data['slideShowSpeed']) 	&& $data['slideShowSpeed'] >= 0) ? $data['slideShowSpeed'] :  $this->slideShowSpeed_default; 

		
		parent::save($data);
	}


}
?>
