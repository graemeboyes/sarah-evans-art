<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

$gal = $this->controller->getSortedImages();
?>
<div class='c5-galleria'>
        <ul id='galleria_<?php    echo $this->controller->bID?>_handler' class="c5Galleria">
        <?php  if (is_array($gal)) { ?>
                <?php     foreach ($gal as $g) : ?>
                        <li>
                                <img src='<?php    echo $g["src"]?>' width='<?php    echo $g["width"]?>' height='<?php    echo $g["height"]?>' title='<?php    echo $g["title"]?>' alt='<?php    echo $g["desc"]?>' rel='<?php    echo $g["desc"]?>'/>
                        </li>
                <?php     endforeach; ?>
        <?php  } ?>
        </ul>
</div>

<script type="text/javascript">

$(document).ready(function(){

                Galleria.loadTheme('<?php    echo $this->getBlockURL()?>/js/themes/classic/galleria.classic.js');
        
                $('ul#galleria_<?php    echo $bID?>_handler').addClass('galleria_<?php    echo $bID?>');
                
                $('ul#galleria_<?php    echo $bID?>_handler').galleria({
                        <?php    echo $width > 0 ? "width:$width,\n" : " "?>
                        <?php    echo $height > 0 ? "height:$height,\n" : " "?>
                        history :               <?php    echo $history ? "true":"false"?>,
                        carousel :              true<?php    //$carousel ? "true":"false"?>,
                        carousel_follow :       <?php    echo $carouselFollow ? "true":"false"?>,
                        carousel_speed :        <?php    echo $carouselSpeed?>,
                        carousel_steps :        '<?php    echo $carouselSteps?>',
                        image_crop :            <?php    echo $imageCrop ? "true":"false"?>,
                        image_margin :          <?php    echo $imageMargin?>,
                        max_scale_ratio :       <?php    echo $maxScaleRatio ? $maxScaleRatio : '0'?>,
                        popup_links :           <?php    echo $popupLinks ? "true":"false"?>,
                        preload :               <?php    echo $preload ? "true":"false"?>,
                        thumbnails :            <?php    echo $thumbnails ? "true":"false"?>,
                        thumb_crop  :           <?php    echo $thumbCrop ? "true":"false"?>,
                        thumb_margin :          <?php    echo $thumb_margin ? $thumb_margin :'0' ?>,
                        transition :            '<?php    echo $transition?>',
                        transition_speed :      <?php    echo $transitionSpeed?>,
                        slide_show  :           <?php    echo $slideShow ? "true":"false"?>,
                        slide_show_speed :      <?php    echo $slideShowSpeed ? $slideShowSpeed : '3000' ?>,
                        my_color :      '<?php    echo $myColor ? $myColor : '#eeeeee' ?>'
                
        });
});

</script>
<!--
-->