<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
	
<!-- Site Header Content //-->
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('main.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />

<?php  Loader::element('header_required'); ?>	
<script src="http://sarahevansart.co.uk/themes/sarah-evans-art/js/script.js" type="text/javascript"></script>

</head>
<body>
<div id="page">
  <div id="header">	
      <div id="header-area">
		<h1 id="logo">
			<a href="#"><?php 
			$a = new Area('Header');
			$a->display($c);
			?></a>
        </h1>
            		<?php 
		if (!$c->isEditMode()) { ?>
			<div class="spacer"></div>
		<?php  } ?>		

		</div>
   </div>