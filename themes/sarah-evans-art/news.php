<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php'); ?>
    <div id="central" class="sidebar-left">
        <div id="sidebar">
			<?php 
			$as = new Area('Sidebar');
			$as->display($c);
			?>		
		</div>
		<div id="body">
        <?php
echo date("F j, Y, g:i a");	?>		<?php 
			$a = new Area('Main');
			$a->display($c);
			?>
		</div>	
		</div>	
		<div class="spacer">&nbsp;</div>		
	</div>
<?php  $this->inc('elements/footer.php'); ?>