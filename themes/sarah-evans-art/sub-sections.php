<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php'); ?>
    <div id="central" class="sub-sections">
        <div id="sidebar">
			<?php 
			$as = new Area('Sidebar');
			$as->display($c);
			?>		
		</div>
        <div id="body">
        	<h1><?php print $c ->getCollectionName (); ?></h1>
							<?php 
								$a = new Area('Main');
								$a->display($c);
							?>
            <div class="column" id="col1">
                <?php 
                $as = new Area('Column 1');
                $as->display($c);
                ?>		
            </div>
            <div class="column" id="col2">
                <?php 
                  $a = new Area('Column 2');
                  $a->display($c);
                ?>
            </div>
            <div class="column" id="last">
                <?php 
                  $a = new Area('Column 3');
                  $a->display($c);
                ?>
            </div>
        </div>
		<div class="spacer">&nbsp;</div>		
	</div>
<?php  $this->inc('elements/footer.php'); ?>