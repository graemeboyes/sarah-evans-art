<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
	
<!-- Site Header Content //-->
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('main.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />

<?php  Loader::element('header_required'); ?>	

</head>
<body class="background2">
<div id="page">
  <div id="header">	
      <div id="header-area">
		<h1 id="logo"><!--
			--><a href="<?php echo DIR_REL?>/"><?php 
				$block = Block::getByName('My_Site_Name');
				if( $block && $block->bID ) $block->display();  
				else echo SITE;
			?></a><!--
		--></h1>
        <div id="headerNav">
			<?php 			
			$ah = new Area('Header Nav');
			$ah->display($c);			
			?>	
       </div>     
            		<?php 
		if (!$c->isEditMode()) { ?>
			<div class="spacer"></div>
		<?php  } ?>		

		</div>
   </div>
    <div id="central" class="contact">
        <div id="sidebar">
			<?php 
			$as = new Area('Sidebar');
			$as->display($c);
			?>		
		</div>
		<div id="body">
			<?php 
			$a = new Area('Main');
			$a->display($c);
			?>
		</div>	
		<div class="spacer">&nbsp;</div>		
	</div>
<?php  $this->inc('elements/footer.php'); ?>