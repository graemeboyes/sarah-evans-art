<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> <head>
	
<!-- Site Header Content //-->
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('main.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />

<?php  Loader::element('header_required'); ?>	
<script src="http://localhost:8888/sarahevansart/themes/sarah-evans-art/js/script.js" type="text/javascript"></script>
</head>
<body>
<div id="page" class="full">
  <div id="header">	
      <div id="header-area">
		<h1 id="logo">
			<a href="http://sarahevansart.co.uk"><img src="http://sarahevansart.co.uk/themes/sarah-evans-art/images/header-small.jpg" width="940" height="30" alt="" /></a>
        </h1>
		</div>
		<div class="page-title">
			<h1><?php echo $c->getCollectionName() ?></h1>  
		</div>
   </div>    
   <div id="central" class="no-sidebar">
		<div id="body">
   			<?php 
			$a = new Area('Main');
			$a->display($c);
			?>
			<a href="javascript:history.go(-1)" class="back-button">&#60;&#60;Back</a>
			<?php
			$attr = $c->getAttribute('image_info');
			if (!empty($attr)): ?>
		   	<a href="#" class="ainfo"><img src="http://sarahevansart.co.uk/themes/sarah-evans-art/images/info-icon.gif" alt="" width="25" height="25" /></a>
   			<div class="image-info">
   				<p class="title"><?php echo $c->getCollectionAttributeValue('image_info') ?></p>
   			</div>
   			<?php endif; ?>
		</div>	
	</div>
</div>
	<div class="bottom-nav">
			<?php 
			$a = new Area('Bottom Navigation');
			$a->display($c);
			?>
	</div>
	<div id="footer">
			&copy; <?php echo date('Y')?> <a href="<?php echo DIR_REL?>/"><?php echo SITE?></a>.
			&nbsp;&nbsp;
			<?php echo t('All rights reserved.')?>	
			<?php 
			$u = new User();
			if ($u->isRegistered()) { ?>
				<?php  
				if (Config::get("ENABLE_USER_PROFILES")) {
					$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
				} else {
					$userName = $u->getUserName();
				}
				?>
				<span class="sign-in"><?php echo t('Currently logged in as <b>%s</b>.', $userName)?> <a href="<?php echo $this->url('/login', 'logout')?>"><?php echo t('Sign Out')?></a></span>
			<?php  } else { ?>
				<span class="sign-in"><a href="<?php echo $this->url('/login')?>"><?php echo t('Sign In to Edit this Site')?></a></span>
			<?php  } ?>
	</div>
<script type="text/javascript">
    $(".image-info").hide();
    $(".ainfo").click(function ( event ) {
      event.preventDefault();
      $(".image-info").toggle();
    });
</script>
</body>
</html>
