<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
	
<!-- Site Header Content //-->
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('main.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />

<?php  Loader::element('header_required'); ?>	

</head>
<body>
<div id="page" class="home">
  <div id="header">	
      <div id="header-area">
		<h1 id="logo">
			<a href="http://sarahevansart.co.uk"><?php 
			$a = new Area('Header');
			$a->display($c);
			?></a>
        </h1>
            		<?php 
		if (!$c->isEditMode()) { ?>
			<div class="spacer"></div>
		<?php  } ?>		

		</div>
   </div>    
   <div id="central" class="home">
		<div id="body">
			<?php 
			$a = new Area('Main');
			$a->display($c);
			?>
		</div>	
	</div>
    <div class="enter"><h3><a href="http://sarahevansart.co.uk/index.php/welcome">Enter</a></h3></div>
<div class="spacer">&nbsp;</div>		
<?php  $this->inc('elements/footer.php'); ?>